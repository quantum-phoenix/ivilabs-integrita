Integrita wordpress default theme
========
## Hungarian ##

Az integrita egy alap téma a Wordpress -hez.

A program a gpl 3.0 licence alatt lett kiadva, a forráskód használatakor kérlek ezt vedd figyelembe.

## English ##

Integrita is a default theme for Wordpress.

The program has been released under the GPL 3.0 license. When you use the source code, please notice this.

## Demo ##

Demo site: Offline now

## Coding standards ##

Url: http://support.envato.com/index.php?/Knowledgebase/Article/View/472/85/wordpress-theme-submission-requirements