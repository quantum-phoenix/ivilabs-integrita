<?php
/**
 * Template Name: Search Template
 * Description: The Search Template
 *
 * @package Integrita default theme
 * @author Rubidium Style Team
 * @link http://rubidium-style.com/
 */
?>

<?php get_header(); ?>

<!-- CONTENT-CONTAINER -->
<div class="container">

    <!-- CONTENT -->
    <div class="content">

        <!-- ROW -->
        <div class="row">

            <!-- COLUMN 8 -->
            <div class="col-md-8">

                <!-- START OF LOOP -->
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <!-- POST CONTAINER -->
                    <div class="post-container">

                        <!-- TITLE-DIV -->
                        <div class="title-div">
                            <h1 class="title">
                                <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                            </h1>
                        </div>
                        <!-- /TITLE-DIV -->

                        <!-- POST-$ID -->
                        <div id="post-<?php the_ID(); ?>" <?php post_class('post-div') ?>>
                            <div class="post-entry">
                                <?php the_content(); ?>
                            </div>
                        </div>
                        <!-- /POST-$ID -->

                    </div>
                    <!-- /POST CONTAINER -->

                <?php endwhile; ?>

                    <!-- NAV -->
                    <div id="nav-above" class="navigation blog-navigation">
                        <div class="nav-previous clearfix">
                            <?php next_posts_link(__('<p>Older posts</p><span class="meta-nav-new"></span>')); ?>
                        </div>

                        <div class="nav-next clearfix">
                            <?php previous_posts_link(__('<p>Newer posts</p><span class="meta-nav-old"></span>')); ?>
                        </div>
                    </div>
                    <!-- /NAV -->

                <?php endif; ?>
                <!-- /END OF LOOP -->

            </div>
            <!-- /COLUMN 8 -->

            <!-- COLUMN 4 -->
            <div class="sidebars col-md-4">
                <?php get_sidebar('posts'); ?>
            </div>
            <!-- /COLUMN 4 -->

        </div>
        <!-- /ROW -->

    </div>
    <!-- /CONTENT -->

</div>
<!-- /CONTENT-CONTAINER -->

<?php get_footer(); ?>