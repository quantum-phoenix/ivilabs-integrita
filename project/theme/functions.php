<?php

/* -----------------------------------------------------
Language file setup
----------------------------------------------------- */
add_action('after_setup_theme', 'language_setup');
function language_setup(){
    $path = get_template_directory() . '/core/languages';
    $result = load_theme_textdomain('integrita', $path );
    if ( $result ){
        return; 
    }  else{
        $locale = apply_filters( 'theme_locale', get_locale(), 'integrita' );
        echo "Could not find $path/$locale.mo." ;      
    }
}
/* -----------------------------------------------------
Custom Menu
----------------------------------------------------- */
add_action('init', 'register_custom_menu');
function register_custom_menu()
{
    register_nav_menu('custom_menu', __('Custom Menu', 'integrita'));
}
/* -----------------------------------------------------
Post format
----------------------------------------------------- */
add_action( 'after_setup_theme', 'add_post_formats', 20 );
function add_post_formats() {
    add_theme_support('post-formats', array('video'));
}

/* -----------------------------------------------------
Post limit
----------------------------------------------------- */
add_filter('pre_option_posts_per_page', 'limit_posts_per_page');
function limit_posts_per_page()
{
    if (is_category())
        return 3;
    else
        return 5; // default: 5 posts per page
}
/* -----------------------------------------------------
Breadcrumb navigation
----------------------------------------------------- */
function the_breadcrumb() {
        echo '<ul class="int-breadcrumb-crumbs">';
        if (!is_home()) {
                echo '<li><a href="';
                echo get_option('home');
                echo '">';
                echo 'Home';
                echo '</a><i class="fa fa-angle-right"></i></li>';
                if (is_category() || is_single()) {
                        echo '<li>';
                        the_category(' </li><li> ');
                        if (is_single()) {
                                echo "</li><li>";
                                the_title();
                                echo '</li>';
                        }
                } elseif (is_page()) {
                        echo '<li>';
                        echo the_title();
                        echo '</li>';
                }
        }
        elseif (is_tag()) {single_tag_title();}
        elseif (is_day()) {echo"<li>Archive for "; the_time('F jS, Y'); echo'</li>';}
        elseif (is_month()) {echo"<li>Archive for "; the_time('F, Y'); echo'</li>';}
        elseif (is_year()) {echo"<li>Archive for "; the_time('Y'); echo'</li>';}
        elseif (is_author()) {echo"<li>Author Archive"; echo'</li>';}
        elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
        elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}
        echo '</ul>';
}
/* -----------------------------------------------------
Thumbnails
----------------------------------------------------- */
if (function_exists('add_image_size')) {
    add_theme_support('post-thumbnails');
}

if (function_exists('add_image_size')) {
    $aspect_ratio = 0.35;
    /* Old ******/
    add_image_size('blog-large-full-thumb', 1140, 400, true);
    add_image_size('blog-large-sidebar-thumb', 750, 230, true);
    add_image_size('home-posts-thumb', 235, 130, true);
    add_image_size('search-thumb', 980, 230, true);

    /* New ******/
    add_image_size('list-blog-with-sidebar', 848, 848*$aspect_ratio, true);
    add_image_size('list-blog-without-sidebar', 1140, 1140*$aspect_ratio, true);

    add_image_size('masonry-blog-two-column', 570, 570, true);
    add_image_size('masonry-blog-three-column', 380, 380, true); 
    add_image_size('masonry-blog-four-column', 285, 285, true);

    add_image_size('grid-portfolio-two-column', 554, 554, true);
    add_image_size('grid-portfolio-three-column', 364, 364, true);
    add_image_size('grid-portfolio-four-column', 269, 269, true);

    add_image_size('selected-post-types-one-column', 1110, 1110*$aspect_ratio, true);
    add_image_size('selected-post-types-two-column', 540, 540*$aspect_ratio, true);
    add_image_size('selected-post-types-three-column', 350, 350, true);
    add_image_size('selected-post-types-four-column', 255, 255, true);
    add_image_size('selected-post-types-six-column', 165, 165, true);    
}

/* -----------------------------------------------------
Widgets shortcodes ON
----------------------------------------------------- */
add_filter('widget_text', 'shortcode_unautop');
add_filter('widget_text', 'do_shortcode');
/* -----------------------------------------------------
Excerpt
----------------------------------------------------- */
add_filter('excerpt_length', 'integrita_excerpt_length');
function integrita_excerpt_length($length) {
    return 50;
}

add_filter('wp_trim_excerpt', 'integrita_excerpt_more');
function integrita_excerpt_more($excerpt) {
    return str_replace('[...]', '', $excerpt);
}
/* -----------------------------------------------------
Remove Autop
----------------------------------------------------- */
function rm_wpautop($content) {
    global $post;
    // Remove the filter
    remove_filter('the_content', 'wpautop');
    return $content;
}

// Hook into the Plugin API
add_filter('the_content', 'rm_wpautop', 9);

/* -----------------------------------------------------
Javascripts and css loader
----------------------------------------------------- */
require_once(get_stylesheet_directory() . '/core/functions/js_and_css_loader.php');

/* -----------------------------------------------------
Widgets
----------------------------------------------------- */
require_once(get_stylesheet_directory() . '/core/functions/widgets.php');

/* -----------------------------------------------------
Better comments
----------------------------------------------------- */
require_once(get_stylesheet_directory() . '/core/functions/comments_override.php');

/* -----------------------------------------------------
Shortcodes
----------------------------------------------------- */
require_once(get_stylesheet_directory() . '/core/functions/shortcodes.php');

/* -----------------------------------------------------
Visual Composer
----------------------------------------------------- */
require_once(get_stylesheet_directory() . '/core/functions/implementation/vc_connector.php');

/* -----------------------------------------------------
Post types
----------------------------------------------------- */
require_once(get_stylesheet_directory() . '/core/functions/post_types.php');

/* -----------------------------------------------------
Metaboxes
----------------------------------------------------- */
require_once(get_stylesheet_directory() . '/core/functions/metaboxes.php');

/* -----------------------------------------------------
TGM Plugin
----------------------------------------------------- */
if(is_admin()){ 
    require_once(get_stylesheet_directory() . '/core/functions/libs/tgm_use.php');
}

/* -----------------------------------------------------
Options Framework
----------------------------------------------------- */
require_once(get_stylesheet_directory() . '/core/functions/libs/options_use.php');

/* -----------------------------------------------------
BFI Thumb
----------------------------------------------------- */
require_once (get_stylesheet_directory() . '/core/functions/libs/bfi/BFI_Thumb.php');
/* -----------------------------------------------------
Custom search form
----------------------------------------------------- */
add_filter( 'get_search_form', 'integrita_search_form' );
function integrita_search_form( $form ) {
    $form = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url( '/' ) . '" >';
        $form .= '<div class="input-group">';
            $form .= '<input type="text" class="form-control" placeholder="' . get_search_query() . '" name="s" id="s" />';
            $form .= '<span class="input-group-btn"><button class="btn btn-default" type="submit" id="searchsubmit" type="button">'. esc_attr__( 'Search' ) .'</button></span>';
        $form .= '</div>';
    $form .= '</form>';
    return $form;
}
/* -----------------------------------------------------
Custom wp query
----------------------------------------------------- */
add_filter( 'posts_where', 'title_like_posts_where', 10, 2 );
function title_like_posts_where( $where, &$wp_query ) {
    global $wpdb;
    if ( $post_title_like = $wp_query->get( 'post_title_like' ) ) {
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'' . esc_sql( like_escape( $post_title_like ) ) . '%\'';
    }
    return $where;
}


//add_filter( 'posts_request', 'dump_request' );
function dump_request( $input ) {
    echo '<pre>';
    var_dump($input);
    echo '</pre>';
    return $input;
}
