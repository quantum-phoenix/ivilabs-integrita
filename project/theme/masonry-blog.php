<?php
/**
 * Template Name: Masonry Blog Layout Template  
 */
?>

<?php get_header(); ?>

<!-- CONTENT-CONTAINER -->
<div class="container">

    <!-- CONTENT -->
    <div class="content">
          
            <!-- ROW -->
            <div class="row">

                <!-- COLUMN 12 -->
                <div class="col-md-12">

                    <!-- START OF LOOP -->
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>                        

                        <!-- PAGE-CONTAINER -->
                        <div class="page-container">

                            <!-- TITLE-DIV -->
                            <div class="title-div">                                
                                <h3><?php the_title(); ?></h3>                           
                            </div>
                            <!-- /TITLE-DIV -->

                        </div>
                        <!-- /PAGE-CONTAINER -->

                        <!-- MASONRY BLOG CONTAINER -->
                        <div class="masonry-blog-container clearfix">
                            <?php get_template_part( 'core/templates/blog-masonry', 'content' ); ?>
                        </div>
                        <!-- /MASONRY BLOG CONTAINER -->

                    <?php endwhile; ?>

                        <!-- NAV -->
                        <div id="nav-above" class="navigation blog-navigation">
                            <div class="nav-previous">
                                <?php next_posts_link(__('<p>Older posts</p><span class="meta-nav-new"></span>')); ?>
                            </div>

                            <div class="nav-next">
                                <?php previous_posts_link(__('<p>Newer posts</p><span class="meta-nav-old"></span>')); ?>
                            </div>
                        </div>
                        <!-- /NAV -->

                    <?php endif; ?>
                    <!-- /END OF LOOP -->

                </div>
                <!-- /COLUMN 12 -->

            </div>
            <!-- /ROW -->

    </div>
    <!-- /CONTENT -->

</div>
<!-- /CONTENT-CONTAINER -->

<?php get_footer(); ?>