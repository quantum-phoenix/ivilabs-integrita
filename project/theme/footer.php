<!-- FOOTER-CONTAINER -->
<footer class="footer-container">

    <!-- FOOTER-SIDEBAR-WRAPPER -->
    <div class="container">
        <?php get_sidebar('footer'); ?>
    </div>
    <!-- /FOOTER-SIDEBAR-WRAPPER -->

</footer>
<!-- /FOOTER-CONTAINER -->

<!-- WRAPPER -->
</div>
<!-- /WRAPPER -->

<!-- THEME HOOK -->
<?php wp_footer(); ?>
<!-- /THEME HOOK -->

</body>
</html>