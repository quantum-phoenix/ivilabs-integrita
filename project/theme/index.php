<?php
/**
 * Template Name: List Blog Without Sidebar
 */
?>

<?php get_header(); ?>

<!-- ROW -->
<div class="row head-content">

    <!-- CONTENT-CONTAINER -->
    <div class="container">

        <!-- COLUMN 6 -->
        <div class="col-md-6">
            <div class="title-div">
            <?php if (is_front_page()): ?>
                <h3 class="no-margin"><?php _e("Blog", "integrita") ?></h3> 
            <?php else:?>                            
                <h3 class="no-margin"><?php the_title(); ?></h3>
            <?php endif; ?>                               
            </div>
        </div>
        <!-- /COLUMN 6 -->

        <!-- COLUMN 6 -->
        <div class="int-breadcrumb col-md-6">
            <?php the_breadcrumb(); ?>
        </div>
        <!-- /COLUMN 6 -->
        
    </div>
    <!-- /CONTENT-CONTAINER -->

</div>
<!-- /ROW -->

<!-- CONTENT-CONTAINER -->
<div class="container">

    <!-- CONTENT -->
    <div class="content">
          
            <!-- ROW -->
            <div class="row">
                
                <!-- COLUMN 12 -->
                <div class="col-md-12">

                    <?php if (is_front_page()): ?>                        
                        <!-- PAGE-CONTAINER -->
                        <div class="page-container">
                            <?php get_template_part( 'core/templates/blog-list', 'content' ); ?>
                        </div>
                        <!-- /PAGE-CONTAINER -->
                    <?php else:?>
                    <!-- START OF LOOP -->
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <!-- PAGE-CONTAINER -->
                        <div class="page-container">
                            <?php get_template_part( 'core/templates/blog-list', 'content' ); ?>
                        </div>
                        <!-- /PAGE-CONTAINER -->
                    <?php endwhile; ?>
                    <?php endif; ?>
                    <!-- /END OF LOOP -->
                    <?php endif; ?>

                </div>
                <!-- /COLUMN 12 -->

            </div>
            <!-- /ROW -->

    </div>
    <!-- /CONTENT -->

</div>
<!-- /CONTENT-CONTAINER -->

<?php get_footer(); ?>