<?php get_header(); ?>

<!-- CONTENT-CONTAINER -->
<div class="container">

    <!-- CONTENT -->
    <div class="content">

        <!-- ROW -->
        <div class="row">

            <!-- COLUMN 12 -->
            <div id="breadcrumb" class="breadcrumb col-md-12">
                <?php the_breadcrumb(); ?>
            </div>
            <!-- COLUMN 12 -->

        </div>
        <!-- /ROW -->

        <!-- ROW -->
        <div class="row">

            <!-- COLUMN 8 -->
            <div class="col-md-8">

                <!-- START OF LOOP -->
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <!-- PAGE CONTAINER -->
                    <div class="post-container">
                        <?php get_template_part( 'core/templates/single', 'content' ); ?>
                    </div>
                    <!-- /PAGE CONTAINER -->

                <?php endwhile; ?>
                <?php endif; ?>
                <!-- /END OF LOOP -->

                <?php comments_template('', true); ?>

            </div>
            <!-- /COLUMN 8 -->

            <!-- COLUMN 4 -->
            <div class="sidebars col-md-4">
                <?php get_sidebar('posts'); ?>
            </div>
            <!-- /COLUMN 4 -->

        </div>
        <!-- /ROW -->

    </div>
    <!-- /CONTENT -->

</div>
<!-- /CONTENT-CONTAINER -->

<?php get_footer(); ?>