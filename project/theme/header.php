<!doctype html>
<head>

    <!--[if lt IE 9]>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/core/js/html5shiv.js"></script>
    <![endif]-->
    
    <!-- META TAGS -->
    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>"/>
    <!-- /META TAGS -->

    <!-- TITLE -->
    <title><?php wp_title(); ?></title>
    <!-- /TITLE -->

    <!-- FAVICON -->
    <link rel="icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico"  type="image/x-icon"/>
    <link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico"  type="image/x-icon"/>
    <!-- /FAVICON -->

    <!-- THEME HOOK -->
    <?php wp_head(); ?>
    <!-- /THEME HOOK -->

</head>

<body <?php body_class(); ?>>

    <!-- WRAPPER -->
    <div class="wrapper">
    <!-- /WRAPPER -->

        <!-- HEADER-CONTAINER -->
        <header class="header-container">
            
            <div class="container">
                <div class="row vertical-center">
                    <div class="logo col-md-4">
                        <?php $logo_value = of_get_option( 'logo_upload', '' ); ?>
                        <?php if(!empty($logo_value)): ?>
                            <img src="<?php echo $logo_value; ?>" alt="<?php echo basename($logo_value); ?>">
                        <?php else: ?>
                            <h1><?php bloginfo('name'); ?></h1>
                        <?php endif; ?>
                    </div>
                    <?php $menu_array = array(
                        'menu' => 'custom_menu',
                        'container' => 'div',
                        'container_class' => 'menu-main-container',
                        'menu_class' => 'clearfix',
                        'echo' => true,
                        'fallback_cb' => 'wp_page_menu',
                        'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth' => 0
                    );
                    ?>
                    <nav class="nav menu-main-container-parent col-md-8">
                        <?php wp_nav_menu($menu_array); ?>
                    </nav>
                </div>
            </div>            

        </header>
        <!-- /HEADER-CONTAINER -->