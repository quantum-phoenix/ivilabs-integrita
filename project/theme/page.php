<?php
/**
 * Template Name: Page Without Sidebar Template
 */
?>

<?php get_header(); ?>

<!-- ROW -->
<div class="row head-content">

    <!-- CONTENT-CONTAINER -->
    <div class="container">

        <!-- COLUMN 6 -->
        <div class="col-md-6">
            <div class="title-div">                                
                <h3 class="no-margin"><?php the_title(); ?></h3>                                
            </div>
        </div>
        <!-- /COLUMN 6 -->

        <!-- COLUMN 6 -->
        <div class="int-breadcrumb col-md-6">
            <?php the_breadcrumb(); ?>
        </div>
        <!-- /COLUMN 6 -->
        
    </div>
    <!-- /CONTENT-CONTAINER -->

</div>
<!-- /ROW -->

<!-- CONTENT-CONTAINER -->
<div class="container">

    <!-- CONTENT -->
    <div class="content">
    
        <!-- ROW -->
        <div class="row">

            <!-- COLUMN 12 -->
            <div class="col-md-12">

                <!-- START OF LOOP -->
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <!-- PAGE-CONTAINER -->
                    <div class="page-container">

                        <!-- START OF THUMBNAIL LOOP -->
                        <?php if (has_post_thumbnail()) : ?>
                            <div class="blog-thumb">
                                <a href="<?php the_permalink() ?>"><?php the_post_thumbnail('blog-thumb'); ?></a>
                            </div>
                        <?php endif; ?>
                        <!-- /END OF THUMBNAIL LOOP -->

                        <!-- POST-$ID -->
                        <div id="post-<?php the_ID(); ?>" <?php post_class('page-div') ?>>
                            <div class="page-entry">
                                <?php the_content(); ?>
                            </div>
                        </div>
                        <!-- /POST-$ID -->

                    </div>
                    <!-- /PAGE-CONTAINER -->

                <?php endwhile; ?>
                <?php endif; ?>
                <!-- /END OF LOOP -->

            </div>
            <!-- /COLUMN 12 -->      

        </div>
        <!-- /ROW -->

    </div>
    <!-- /CONTENT -->

</div>
<!-- /CONTENT-CONTAINER -->

<?php get_footer(); ?>