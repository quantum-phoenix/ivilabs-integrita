<?php
/**
 * Template Name: List Blog Without Sidebar
 */
?>

<?php get_header(); ?>

<!-- ROW -->
<div class="row head-content">

    <!-- CONTENT-CONTAINER -->
    <div class="container">

        <!-- COLUMN 6 -->
        <div class="col-md-6">
            <div class="title-div">
				<h3 class="no-margin"><?php _e("Blog", "integrita") ?></h3>                              
            </div>
        </div>
        <!-- /COLUMN 6 -->

        <!-- COLUMN 6 -->
        <div class="int-breadcrumb col-md-6">
            <?php the_breadcrumb(); ?>
        </div>
        <!-- /COLUMN 6 -->
        
    </div>
    <!-- /CONTENT-CONTAINER -->

</div>
<!-- /ROW -->

<!-- CONTENT-CONTAINER -->
<div class="container">

    <!-- CONTENT -->
    <div class="content">
          
            <!-- ROW -->
            <div class="row">
                
                <!-- COLUMN 12 -->
                <div class="col-md-12">

						<!-- PAGE-CONTAINER -->
                        <div class="page-container">
                            <?php get_template_part( 'core/templates/blog-list', 'content' ); ?>
                        </div>
                        <!-- /PAGE-CONTAINER -->

                </div>
                <!-- /COLUMN 12 -->

            </div>
            <!-- /ROW -->

    </div>
    <!-- /CONTENT -->

</div>
<!-- /CONTENT-CONTAINER -->

<?php get_footer(); ?>