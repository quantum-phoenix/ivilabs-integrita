<!-- TITLE-DIV -->
<div class="title-div">
    <h1 class="title">
        <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
    </h1>
</div>
<!-- /TITLE-DIV -->

<!-- VIDEO POST FORMAT -->
<?php if ( has_post_format( 'video' ) && get_post_meta(get_the_ID(), 'video_post_format_embed_value', true) != ''): ?>
    <div class="blog-video-container">
        <iframe width="100%" height="315px" src="<?php echo esc_url('https://www.youtube.com/embed/ ' . get_post_meta(get_the_ID(), 'video_post_format_embed_value', true) . '?&showinfo=0&autohide=1'); ?>" frameborder="0" allowfullscreen></iframe>
    </div>
<?php else: ?>
 <!-- START OF THUMBNAIL LOOP -->
<?php if (has_post_thumbnail()) { ?>
    <div class="blog-thumb">
        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('masonry-blog-with-sidebar'); ?></a>
    </div>
<?php } ?>
<!-- /END OF THUMBNAIL LOOP -->
<?php endif; ?>
<!-- /VIDEO POST FORMAT -->

<!-- POST-$ID -->
<div id="post-<?php the_ID(); ?>" <?php post_class('post-div') ?>>
    <div class="excerpt">
        <?php //the_content(); ?>
    </div>
</div>
<!-- /POST-$ID -->