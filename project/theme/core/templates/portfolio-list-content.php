<?php
$sidebar_position = get_post_meta( $post->ID, '_wp_page_template', true );
$thumbnail_size = ( $sidebar_position == "list-portfolio-without-sidebar.php" ? array('width' => 1140, 'height' => 1140*0.35) : array('width' => 848, 'height' => 848*0.35) );

$display_count = 2;
$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1; 
$paged_args = array(
  'post_type' => 'portfolio',
  'orderby' => 'date',
  'order' => 'DESC',
  'post_status' => 'publish',
  'posts_per_page' => $display_count,
  'paged' => $paged
);

$args = array( 'post_type' => 'portfolio', 'posts_per_page' => 5, 'post_status' => 'publish', 'orderby' => 'post_date', 'order' => 'DESC');
$loop = new WP_Query( $paged_args );
?>
<?php if ($loop->have_posts()) : while ( $loop->have_posts() ) : $loop->the_post(); ?>                          
    
    <!-- POST-CONTAINER -->
    <article id="post-<?php the_ID(); ?>" <?php post_class('post-container'); ?>>

        <?php $portfolio_multi_image = get_post_meta(get_the_ID(), 'portfolio_multi_image_data', true); ?>
        <?php if(!empty($portfolio_multi_image)): ?>
        <div>  
            <?php
            $img_src = $portfolio_multi_image['image_url'][0];
            $thumb_image = bfi_thumb($img_src, $thumbnail_size);
            ?>
            <a href="<?php the_permalink() ?>">
                <div class="int-image-hover-parent">
                    <img src="<?php echo $thumb_image; ?>" alt="<?php echo basename($thumb_image); ?>">
                    <div class="int-image-hover">
                        <a href="<?php the_permalink(); ?>"><i class="fa fa-search-plus fa-5"></i></a>
                    </div>
                </div>
            </a>
        </div>
        <?php endif; ?>

        <!-- TITLE-DIV -->
        <div class="title-div">            
            <a href="<?php the_permalink() ?>"><h4 class="title"><?php the_title(); ?></h4></a>            
        </div>
        <!-- /TITLE-DIV -->          

        <!-- EXCERPT -->
        <div class="excerpt">
            <?php echo strip_shortcodes(wp_trim_words($post->post_content, 100 )); ?>
        </div>       
        <!-- /EXCERPT -->

        <!-- READ MORE BUTTON -->
        <div class="read-more-button">
            <a class="btn btn-default" href="<?php echo esc_url(the_permalink()); ?>"><?php _e( "Read More" , 'integrita') ?></a>
        </div>                        
        <!-- /READ MORE BUTTON -->

    </article>
    <!-- /POST-CONTAINER -->

<?php endwhile; ?>
<nav>
    <ul>
        <li><?php previous_posts_link( '&laquo; PREV', $inner_loop->max_num_pages) ?></li> 
        <li><?php next_posts_link( 'NEXT &raquo;', $inner_loop->max_num_pages) ?></li>
    </ul>
</nav>
<?php endif; ?>
<?php wp_reset_postdata(); ?>