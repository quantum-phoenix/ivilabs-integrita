<?php 
$column_count_value = of_get_option( 'masonry_blog_column_count_select', 'two_column' );
$column_count =  $column_count_value . '-masonry-blog-item';

$thumbnail_size = '';
if($column_count_value == 'two_column'):
    $thumbnail_size = 'masonry-blog-two-column';
elseif($column_count_value == 'three_column'):
    $thumbnail_size = 'masonry-blog-three-column';
elseif($column_count_value == 'four_column'):
    $thumbnail_size = 'masonry-blog-four-column';
else:
    $thumbnail_size = 'masonry-blog-two-column'; 
endif;

$args = array( 'posts_per_page' => 5, 'post_status' => 'publish', 'orderby' => 'post_date', 'order' => 'DESC');
$blog_posts = get_posts($args);
foreach($blog_posts as $post) : setup_postdata( $post );
?>
<!-- POST-CONTAINER -->
<article id="post-<?php the_ID(); ?>" <?php post_class('post-container masonry-blog-post-item ' . $column_count); ?>>

    <!-- VIDEO POST FORMAT -->
    <?php if ( has_post_format( 'video' ) && get_post_meta(get_the_ID(), 'video_post_format_embed_value', true) != ''): ?>
        <div class="blog-video-container">
            <iframe width="100%" height="315px" src="<?php echo esc_url('https://www.youtube.com/embed/ ' . get_post_meta(get_the_ID(), 'video_post_format_embed_value', true) . '?&showinfo=0&autohide=1'); ?>" frameborder="0" allowfullscreen></iframe>
        </div>
    <?php else: ?>
     <!-- START OF THUMBNAIL LOOP -->
    <?php if (has_post_thumbnail()) : ?>
        <div class="blog-thumb">
            <a href="<?php the_permalink() ?>"><?php the_post_thumbnail($thumbnail_size); ?></a>
        </div>
    <?php endif; ?>
    <!-- /END OF THUMBNAIL LOOP -->
    <?php endif; ?>
    <!-- /VIDEO POST FORMAT -->
   
    <!-- TITLE-DIV -->
    <div class="title-div">
        <h1 class="title">
            <a href="<?php the_permalink() ?>"><h4><?php the_title(); ?></h4></a>
        </h1>
    </div>
    <!-- /TITLE-DIV -->          

    <!-- TIME AND CATEGORIES -->
    <div class="blog-details">
        <?php 
            $all = __(' Comments', 'integrita');
            $one = __(' Comment', 'integrita');
        ?>  

        <?php $categories = get_the_category(get_the_ID()); ?>
        <?php $output = ''; ?>
        <?php foreach ( $categories as $category ) : ?>
            <?php if($category->name != 'Uncategorized'):?>
            <?php $output .= '<a href="' . esc_url(get_category_link( $category->term_id )) . '" >' . esc_attr($category->name) . '</a>' . ', '; ?>
            <?php endif;?>
        <?php endforeach; ?>
        <?php echo trim($output, ', '); ?>

        <span class="blog-details-author"><?php echo  _e('Posted By:','integrita')?><a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><?php esc_attr(the_author_meta( 'display_name' )); ?></a></span>
        <span class="blog-details-date"><?php echo get_the_time('j') . ' ' . get_the_time('M') . ', ' . get_the_time('Y'); ?></span>
        <span class="blog-details-comments"><?php comments_number( '0 ' . $all, '1 ' . $one, '% ' . $all); ?></span>   
    </div>
    <!-- /TIME AND CATEGORIES -->

    <!-- POST-$ID -->
    <div id="post-<?php the_ID(); ?>" <?php post_class('post-div') ?>>
        <div class="excerpt">
            <?php echo strip_shortcodes(wp_trim_words( $post->post_content, 100 )); ?>
        </div>
    </div>
    <!-- /POST-$ID -->
    
    <!-- READ MORE BUTTON -->
    <div class="read-more-button">
        <a class="btn btn-default" href="<?php echo esc_url(the_permalink()); ?>"><?php _e( "Read More" , 'integrita') ?></a>
    </div>                        
    <!-- /READ MORE BUTTON -->
                                
</article>
<!-- /POST-CONTAINER -->

<?php endforeach; ?>
<?php wp_reset_postdata(); ?>