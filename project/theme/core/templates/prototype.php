<?php 
$args = array('posts_per_page' => 10, 'post_status' => 'publish', 'orderby' => 'post_date', 'order' => 'DESC');
$inner_loop = new WP_Query( $args );
$i = 0; 
?>

<p>Ez a prototype.php</p>
<pre><?php //print_r($inner_loop); ?></pre>
<?php if ($inner_loop->have_posts()) : while ( $inner_loop->have_posts() ) : $inner_loop->the_post(); ?>                           
    <?php $i++; ?>
    <!-- POST-CONTAINER -->
    <article id="post-<?php the_ID(); ?>" <?php post_class('post-container wow bounceInLeft'); ?>>
        
        <!-- TITLE-DIV -->
        <div class="title-div">            
            <a href="<?php the_permalink() ?>"><h4 class="title"><?php the_title(); ?></h4></a>            
        </div>
        <!-- /TITLE-DIV -->    

    </article>
    <!-- /POST-CONTAINER -->


<?php endwhile; ?>
<?php echo $i; ?>

<?php if ($inner_loop->max_num_pages > 1): ?>
    <!-- NAV -->
    <div id="nav-above" class="navigation blog-navigation">
        <div class="nav-previous">
            <?php next_posts_link(__('<p>Older posts</p><span class="meta-nav-new"></span>')); ?>
        </div>

        <div class="nav-next">
            <?php previous_posts_link(__('<p>Newer posts</p><span class="meta-nav-old"></span>')); ?>
        </div>
    </div>
    <!-- /NAV -->
<?php endif; ?>

<?php wp_reset_postdata(); ?>
<?php endif; ?>