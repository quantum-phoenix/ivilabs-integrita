<?php 
$sidebar_position = get_post_meta( $post->ID, '_wp_page_template', true );
$thumbnail_size = ( $sidebar_position == "index.php" ? 'list-blog-without-sidebar' : 'list-blog-with-sidebar' );

$display_count = 2;
$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
$paged_args = array(
  'post_type' => 'post',
  'orderby' => 'date',
  'order' => 'DESC',
  'post_status' => 'publish',
  'posts_per_page' => $display_count,
  'paged' => $paged
);

$inner_loop = new WP_Query($paged_args);
?>
<p><?php echo $inner_loop->max_num_pages; ?></p>
<?php if ($inner_loop->have_posts()) : while ( $inner_loop->have_posts() ) : $inner_loop->the_post(); ?>                            

    <!-- POST-CONTAINER -->
    <article id="post-<?php the_ID(); ?>" <?php post_class('post-container wow bounceInLeft'); ?>>

        <!-- VIDEO POST FORMAT -->
        <?php if ( has_post_format( 'video' ) && get_post_meta(get_the_ID(), 'video_post_format_embed_value', true) != ''): ?>
            <div class="blog-video-container">
                <iframe width="100%" height="315px" src="<?php echo esc_url('https://www.youtube.com/embed/ ' . get_post_meta(get_the_ID(), 'video_post_format_embed_value', true) . '?&showinfo=0&autohide=1'); ?>" frameborder="0" allowfullscreen></iframe>
            </div>
        <?php else: ?>
         <!-- START OF THUMBNAIL LOOP -->
        <?php if (has_post_thumbnail()) : ?>
        <div class="blog-thumb">
            <a href="<?php the_permalink() ?>">
                <div class="int-image-hover-parent">
                    <?php the_post_thumbnail($thumbnail_size); ?>
                    <div class="int-image-hover">
                        <a href="<?php the_permalink(); ?>"><i class="fa fa-search-plus fa-5"></i></a>
                    </div>
                </div>
            </a>
        </div>
        <?php endif; ?>
        <!-- /END OF THUMBNAIL LOOP -->
        <?php endif; ?>
        <!-- /VIDEO POST FORMAT -->

        <!-- TITLE-DIV -->
        <div class="title-div">            
            <a href="<?php the_permalink() ?>"><h4 class="title"><?php the_title(); ?></h4></a>            
        </div>
        <!-- /TITLE-DIV -->          

        <!-- TIME AND CATEGORIES, AUTHOR, COMMENTS -->
        <div class="blog-details">
            <?php 
                $all = __(' Comments', 'integrita');
                $one = __(' Comment', 'integrita');
            ?>

            <?php $categories = get_the_category(get_the_ID()); ?>
            <?php $categories_output = ''; ?>
            <?php if(!empty($categories)): ?>
                <?php foreach ( $categories as $category ): ?>
                    <?php if($category->name != 'Uncategorized'): ?>
                        <?php $categories_output .= '<a href="' . esc_url(get_category_link( $category->term_id )) . '" >' . esc_attr($category->name) . '</a>' . ', '; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>

            <?php $tags = get_the_tags(get_the_ID()); ?>
            <?php $tags_output = ''; ?>
            <?php if(!empty($tags)): ?>
                <?php foreach ( $tags as $tag ): ?>                                        
                        <?php $tags_output .= '<a href="' . esc_url(get_tag_link( $tag->term_id )) . '" >' . esc_attr($tag->name) . '</a>' . ', '; ?>                 
                <?php endforeach; ?>
            <?php endif; ?>

            <?php if(!empty($categories_output)): ?>
                <span class="blog-details-cats">
                    <i class="fa fa-folder-open"></i>
                    <?php echo trim($categories_output, ', '); ?>
                </span>
            <?php endif; ?>
            <?php if(!empty($tags_output)): ?>
                <span class="blog-details-tags">
                    <i class="fa fa-cloud"></i>
                    <?php echo trim($tags_output, ', '); ?>
                </span>
            <?php endif; ?>
            <span class="blog-details-author">
                <i class="fa fa-user"></i>
                <?php esc_attr(the_author_meta( 'display_name' )); ?>
            </span>
            <span class="blog-details-date">
                <i class="fa fa-clock-o"></i>
                <?php echo get_the_time('j') . ' ' . get_the_time('M') . ', ' . get_the_time('Y'); ?>
            </span>
            <span class="blog-details-comments">
                <i class="fa fa-comments-o"></i>
                <?php comments_number( '0 ' . $all, '1 ' . $one, '% ' . $all); ?>
            </span>   
        </div>
        <!-- /TIME AND CATEGORIES, AUTHOR, COMMENTS -->

        <!-- EXCERPT -->
        <div class="excerpt">
            <?php echo strip_shortcodes(wp_trim_words($post->post_content, 100 )); ?>
        </div>       
        <!-- /EXCERPT -->

        <!-- READ MORE BUTTON -->
        <div class="read-more-button">
            <a class="btn btn-default" href="<?php echo esc_url(the_permalink()); ?>"><?php _e( "Read More" , 'integrita') ?></a>
        </div>                        
        <!-- /READ MORE BUTTON -->

    </article>
    <!-- /POST-CONTAINER -->

<?php endwhile; ?>

<nav>
    <ul>
        <li><?php previous_posts_link( '&laquo; PREV', $inner_loop->max_num_pages) ?></li> 
        <li><?php next_posts_link( 'NEXT &raquo;', $inner_loop->max_num_pages) ?></li>
    </ul>
</nav>


<?php wp_reset_postdata(); ?>
<?php endif; ?>