<?php
require_once(get_stylesheet_directory() . '/core/functions/implementation/html_parts.php');
/* -----------------------------------------------------
Button shortcode / Use: [button link="https://twitter.com"]Test[/button]
----------------------------------------------------- */
if (!function_exists('int_shortcode_button')) {
    function int_shortcode_button($atts, $content = null)
    {
        $output  = '';
        $default = array(
            'link' => 'http://www.google.com',
            'color' => 'default',
            'size' => '',
            'class' => '',
            'target' => '_blank',
            'block' => 'off'
        );
        extract(shortcode_atts($default, $atts));
        $output = int_bootstrap_button($content, $link, $color, $size, $class, $target, $block);
        return $output;
    }
}
add_shortcode('button', 'int_shortcode_button');
/* -----------------------------------------------------
Colourful text shortcode
----------------------------------------------------- */
if (!function_exists('int_shortcode_colourful_text')) {
    function int_shortcode_colourful_text($atts, $content = null)
    {
        $output  = '';
        $default = array(
            'color' => 'muted'
        );
        extract(shortcode_atts($default, $atts));
        $output = int_contex_colors($content, $color);
        return $output;
    }
}
add_shortcode('colourful_text', 'int_shortcode_colourful_text');
/* -----------------------------------------------------
Jquery knob shortcode
----------------------------------------------------- */
if (!function_exists('int_shortcode_jquery_knob')) {
    function int_shortcode_jquery_knob($atts, $content = null)
    {
        $output  = '';
        $default = array(
            'thickness' => '.1',
            'title' => ''
        );
        extract(shortcode_atts($default, $atts));
        $output = int_jquery_knob($value, $thickness, $title);
        return $output;
    }
}
add_shortcode('knob', 'int_shortcode_jquery_knob');
/* -----------------------------------------------------
Heading
----------------------------------------------------- */
if (!function_exists('int_shortcode_heading')) {
    function int_shortcode_heading($atts, $content = null)
    {
        $output  = '';
        $default = array(
            'tag' => 'h1',
            'text' => ''
        );
        extract(shortcode_atts($default, $atts));
        $output = int_heading($tag, $text);
        return $output;
    }
}
add_shortcode('heading', 'int_shortcode_heading');
/* -----------------------------------------------------
Counter
----------------------------------------------------- */
if (!function_exists('int_shortcode_counter')) {
    function int_shortcode_counter($atts, $content = null)
    {
        $output  = '';
        $default = array(
            'tag' => 'h1',
            'min' => '0',
            'max' => '1000',
            'delay' => '10',
            'increment' => '9',
            'prefix' => '',
            'postfix' => '',
            'comment' => '',
            'border' => ''
        );
        extract(shortcode_atts($default, $atts));
        $output = int_counter($tag, $min, $max, $delay, $increment, $prefix, $postfix, $comment, $border);
        return $output;
    }
}
add_shortcode('counter', 'int_shortcode_counter');
/* -----------------------------------------------------
Dismissible alerts / Use: [alert]Alert box![/alert]
----------------------------------------------------- */
if (!function_exists('int_shortcode_dismissible_alert')) {
    function int_shortcode_dismissible_alert($atts, $content = null)
    {
        $output  = '';
        $default = array(
            'color' => 'warning',
            'dismissible' => 'off'
        );
        extract(shortcode_atts($default, $atts));
        $output = int_dismissible_alert($content, $color, $dismissible);
        return $output;
    }
}
add_shortcode('alert', 'int_shortcode_dismissible_alert');
/* -----------------------------------------------------
Progress bar / Use: [progress]
----------------------------------------------------- */
if (!function_exists('int_shortcode_progress_bar')) {
    function int_shortcode_progress_bar($atts, $content = null) {
        $output  = '';
        $default = array(
            'value' => '',
            'color' => 'success',
            'align' => '',
            'striped' => 'off',
            'animated' => 'off'
        );
        extract(shortcode_atts($default, $atts));
        $output = int_progress_bar($value, $color, $align, $striped, $animated);
        return $output;
    }
}
add_shortcode('progress', 'int_shortcode_progress_bar');
/* -----------------------------------------------------
Blockquote / Use: [blockquote]10[/blockquote]
----------------------------------------------------- */
if (!function_exists('int_shortcode_blockquote')) {
    function int_shortcode_blockquote($atts, $content = null)
    {
        $output  = '';
        $default = array(
            'reverse' => 'off'
        );
        extract(shortcode_atts($default, $atts));
        $output = int_blockquote($content, $reverse);
        return $output;
    }
}
add_shortcode('blockquote', 'int_shortcode_blockquote');
/* -----------------------------------------------------
Dropcap / Use: [dropcap]a[/dropcap]
----------------------------------------------------- */
if (!function_exists('int_shortcode_dropcap')) {
    function int_shortcode_dropcap($atts, $content = null)
    {
        $output = '';
        $output = int_dropcap($content);
        return $output;
    }
}
add_shortcode('dropcap', 'int_shortcode_dropcap');
/* -----------------------------------------------------
Social / Use: [social class="twitter" link="https://twitter.com"]Twitter[/social]
----------------------------------------------------- */
if (!function_exists('int_shortcode_social_icons')) {
    function int_shortcode_social_icons($atts, $content = null)
    {
        $output  = '';
        $default = array(
            'class' => 'twitter',
            'link' => '#',
            'target' => '_blank'
        );
        extract(shortcode_atts($default, $atts));
        $output = int_social_icons($content, $class, $link, $target);
        return $output;
    }
}
add_shortcode('social', 'int_shortcode_social_icons');
/* -----------------------------------------------------
Google map / Use: [map position_latitude="56.948813" position_longitude="24.704004" marker_latitude="56.948813" marker_longitude="24.704004" title="Test"]
----------------------------------------------------- */
if (!function_exists('int_shortcode_google_map')) {
    function int_shortcode_google_map($atts, $content = null)
    {
        $output  = '';
        $default = array(
            'position_latitude' => '',
            'position_longitude' => '',
            'marker_latitude' => '',
            'marker_longitude' => '',
            'title' => ''
        );
        extract(shortcode_atts($default, $atts));
        $output = int_google_map($position_latitude, $position_longitude, $marker_latitude, $marker_longitude, $title);
        return $output;
    }
}
add_shortcode('map', 'int_shortcode_google_map');
/* -----------------------------------------------------
Countent box / Use: [content_box title="Test"]Test[/content_box]
----------------------------------------------------- */
if (!function_exists('int_shortcode_content_box')) {
    function int_shortcode_content_box($atts, $content = null)
    {
        $output  = '';
        $default = array(
            'icon' => 'home',
            'title' => 'Title here',
            'link' => '#',
            'background' => ''
        );
        extract(shortcode_atts($default, $atts));
        $output = int_content_box($content, $icon, $title, $link, $background);
        return $output;
    }
}
add_shortcode('content_box', 'int_shortcode_content_box');
/* -----------------------------------------------------
Pricing table / Use: [pricing_table title="Test"]Test[/pricing_table]
----------------------------------------------------- */
if (!function_exists('int_shortcode_pricing_table')) {
    function int_shortcode_pricing_table($atts, $content = null)
    {
        $output  = '';
        $default = array(
            'title' => 'Title here',
            'prefix' => '$',
            'normalprice' => '1',
            'decimalprice' => '99',
            'postfix' => 'monthly',
            'button_text' => 'Button',
            'button_link' => '#'
        );
        extract(shortcode_atts($default, $atts));
        $output = int_pricing_table($content, $title, $prefix, $normalprice, $decimalprice, $postfix, $button_text, $button_link);
        return $output;
    }
}
add_shortcode('pricing_table', 'int_shortcode_pricing_table');
/* -----------------------------------------------------
Team member / Use:
----------------------------------------------------- */
if (!function_exists('int_shortcode_team_member')) {
    function int_shortcode_team_member($atts, $content = null)
    {
        $output  = '';
        $default = array(
            'name' => ''
        );        
        extract(shortcode_atts($default, $atts));
        $output = int_team_member($name);
        return $output;
    }
}
add_shortcode('team_member', 'int_shortcode_team_member');
/* -----------------------------------------------------
Youtube / Use: [youtube video_id="QfQduliOetc"]
----------------------------------------------------- */
if (!function_exists('int_shortcode_youtube')) {
    function int_shortcode_youtube($atts, $content = null)
    {
        $output  = '';
        $default = array(
            'video_id' => '',
            'width' => '420',
            'height' => '315'
        );
        extract(shortcode_atts($default, $atts));
        $output = int_youtube($video_id, $width, $height);
        return $output;
    }
}
add_shortcode('youtube', 'int_shortcode_youtube');
/* -----------------------------------------------------
Vimeo / Use: [vimeo video_id="115014610"]
----------------------------------------------------- */
if (!function_exists('int_shortcode_vimeo')) {
    function int_shortcode_vimeo($atts, $content = null)
    {
        $output  = '';
        $default = array(
            'video_id' => '',
            'width' => '420',
            'height' => '315'
        );
        extract(shortcode_atts($default, $atts));
        $output = int_vimeo($video_id, $width, $height);
        return $output;
    }
}
add_shortcode('vimeo', 'int_shortcode_vimeo');
/* -----------------------------------------------------
Tagline Box / Use: [tagline_box title="Example"]Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, est ex quod ipsum perferendis, adipisci fugiat, a qui modi voluptate laborum eius aliquid nam similique.[/tagline_box]
----------------------------------------------------- */
if (!function_exists('int_shortcode_tagline_box')) {
    function int_shortcode_tagline_box($atts, $content = null)
    {
        $output  = '';
        $default = array(
            'title' => '',
            'color' => 'cccccc',
            'border_position' => 'top',
            'border_position_width' => '3',
            'all_border_width' => '0',
            'button_text' => '',
            'button_link' => '#'
        );
        extract(shortcode_atts($default, $atts));
        $output = int_tagline_box($content, $title, $color, $border_position, $border_position_width, $all_border_width, $button_link, $button_text);
        return $output;
    }
}
add_shortcode('tagline_box', 'int_shortcode_tagline_box');
/* -----------------------------------------------------
Popover / Use: [popover title="Example" button_text="Example"]Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, est ex quod ipsum perferendis, adipisci fugiat, a qui modi voluptate laborum eius aliquid nam similique.[/popover]
----------------------------------------------------- */
if (!function_exists('int_shortcode_popover')) {
    function int_shortcode_popover($atts, $content = null)
    {
        $output  = '';
        $default = array(
            'title' => '',
            'button_link' => '#',
            'button_text' => 'Button text here',
            'placement' => 'top'

        );
        extract(shortcode_atts($default, $atts));
        $output = int_popover($content, $title, $button_link, $button_text, $placement);
        return $output;
    }
}
add_shortcode('popover', 'int_shortcode_popover');

/* -----------------------------------------------------
Selected post types / Use: 
----------------------------------------------------- */
if (!function_exists('int_shortcode_list_selected_post_types')) {
    function int_shortcode_list_selected_post_types($atts, $content = null)
    {
        $output  = '';
        $default = array(
            'selected_post_type' => '',
            'column' => '',
            'slider' => ''
        );
        extract(shortcode_atts($default, $atts));
        $output = int_list_selected_post_types($selected_post_type, $column, $slider);
        return $output;
    }
}
add_shortcode('post_types_display', 'int_shortcode_list_selected_post_types');

/* -----------------------------------------------------
Divider / Use: 
----------------------------------------------------- */
if (!function_exists('int_shortcode_divider')) {
    function int_shortcode_divider($atts, $content = null)
    {
        $output  = '';
        $default = array(
            'border_color' => '',
            'margin_top' => '',
            'margin_bottom' => '',
            'border_width' => '',
            'border_styles' => ''
        );
        extract(shortcode_atts($default, $atts));
        $output = int_divider($border_color, $margin_top, $margin_bottom, $border_width, $border_styles);
        return $output;
    }
}
add_shortcode('divider', 'int_shortcode_divider');

/* -----------------------------------------------------
Temporary
----------------------------------------------------- */
if (!function_exists('int_shortcode_list_item')) {
    function int_shortcode_list_item($atts, $content = null)
    {
        $output  = '';
        $output = int_list_item($content);
        return $output;
    }
}
add_shortcode('listitem', 'int_shortcode_list_item');