<?php
/*
 * Loads the Options Panel
 *
 * If you're loading from a child theme use stylesheet_directory
 * instead of template_directory
 */
define('OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/core/functions/libs/options/inc/');
$optionsframework = dirname(__FILE__) . '/options/inc/options-framework.php';

if (is_file($optionsframework)) {	
    require_once($optionsframework);	
} else {
    die("File ${optionsframework} does not exist." . $optionsframework);
}

// Loads options.php from child or parent theme
$optionsfile = locate_template('options.php');
load_template($optionsfile);