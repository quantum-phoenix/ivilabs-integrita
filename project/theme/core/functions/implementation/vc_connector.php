<?php
/* -----------------------------------------------------
Social shortcode
----------------------------------------------------- */
add_action('vc_before_init', 'vc_social');
function vc_social()
{
    vc_map(array(
        "name" => __("Social icons", "integrita"),
        "base" => "social",
        "class" => "",
        "category" => __("Integrita", "integrita"),
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Social icon text", "integrita"),
                "param_name" => "content",
                "value" => "",
                "description" => __("Ha nem akarsz szöveget az ikonhoz hagyd üresen.", "integrita")
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Social icon list.", "integrita"),
                "param_name" => "class",
                "value" => array(
                    "App.net" => "adn",
                    "Bitbucket" => "bitbucket",
                    "Dropbox" => "dropbox",
                    "Facebook" => "facebook",
                    "Flickr" => "flickr",
                    "Foursquare" => "foursquare",
                    "Github" => "github",
                    "Google+" => "google-plus",
                    "Instagram" => "instagram",
                    "Linkedin" => "linkedin",
                    "Openid" => "openid",
                    "Pinterest" => "pinterest",
                    "Reddit" => "reddit",
                    "Soundcloud" => "soundcloud",
                    "Tumblr" => "tumblr",
                    "Twitter" => "twitter",
                    "Yk" => "vk",
                    "Yahoo" => "yahoo"
                ),
                "description" => __("Social icon list.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Link", "integrita"),
                "param_name" => "link",
                "value" => __("#", "integrita"),
                "description" => __("Social icon link.", "integrita")
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Target", "integrita"),
                "param_name" => "target",
                "value" => array(
                    "Blank" => "_blank",
                    "Self" => "_self",
                    "Parent" => "_parent",
                    "Top" => "_top"
                ),
                "description" => __("HTML a tag target attribute", "integrita")
            )
        )
    ));
}
/* -----------------------------------------------------
Button
----------------------------------------------------- */
add_action('vc_before_init', 'vc_button');
function vc_button()
{
    vc_map(array(
        "name" => __("Button", "integrita"),
        "base" => "button",
        "class" => "",
        "category" => __("Integrita", "integrita"),
        "icon" => "fa fa-camera-retro",
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Text on the button.", "integrita"),
                "param_name" => "content",
                "value" => "",
                "description" => __("Text on the button.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("URL (Link)", "integrita"),
                "param_name" => "link",
                "value" => "",
                "description" => __("Button link.", "integrita")
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Color", "integrita"),
                "param_name" => "color",
                "value" => array(
                    "Default" => "default",
                    "Primary" => "primary",
                    "Success" => "success",
                    "Info" => "info",
                    "Warning" => "warning",
                    "Danger" => "danger",
                    "Link" => "link"
                ),
                "description" => __("Button color.", "integrita")
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Size", "integrita"),
                "param_name" => "size",
                "value" => array(
                    "Default" => "",
                    "Extra small" => "xs",
                    "Small" => "sm",
                    "Large" => "lg"
                ),
                "description" => __("Button size.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Class", "integrita"),
                "param_name" => "class",
                "description" => __("Class", "integrita")
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Target", "integrita"),
                "param_name" => "target",
                "value" => array(
                    "Blank" => "_blank",
                    "Self" => "_self",
                    "Parent" => "_parent",
                    "Top" => "_top"
                ),
                "description" => __("A link cél ablaka.", "integrita")
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Button block type", "integrita"),
                "param_name" => "block",
                "value" => array(
                    "Off" => "off",
                    "On" => "on"                    
                ),
                "description" => __("A gomb kitöltse a teljes oldalt?", "integrita")
            )
            
        )
    ));
}
/* -----------------------------------------------------
Youtube
----------------------------------------------------- */
add_action('vc_before_init', 'vc_youtube');
function vc_youtube()
{
    vc_map(array(
        "name" => __("Youtube", "integrita"),
        "base" => "youtube",
        "class" => "",
        "category" => __("Integrita", "integrita"),
        "icon" => "fa fa-camera-retro",
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Video ID", "integrita"),
                "param_name" => "video_id",
                "value" => "",
                "description" => __("A youtube video, video id -je.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Video width.", "integrita"),
                "param_name" => "width",
                "value" => "420",
                "description" => __("Video width.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Video height.", "integrita"),
                "param_name" => "height",
                "value" => "315",
                "description" => __("Video height.", "integrita")
            )

            
        )
    ));
}
/* -----------------------------------------------------
Vimeo
----------------------------------------------------- */
add_action('vc_before_init', 'vc_vimeo');
function vc_vimeo()
{
    vc_map(array(
        "name" => __("Vimeo", "integrita"),
        "base" => "vimeo",
        "class" => "",
        "category" => __("Integrita", "integrita"),
        "icon" => "fa fa-camera-retro",
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Video ID", "integrita"),
                "param_name" => "video_id",
                "value" => "",
                "description" => __("A vimeo video, video id -je.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Video width.", "integrita"),
                "param_name" => "width",
                "value" => "420",
                "description" => __("Video width.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Video height.", "integrita"),
                "param_name" => "height",
                "value" => "315",
                "description" => __("Video height.", "integrita")
            )            
        )
    ));
}
/* -----------------------------------------------------
Tagline box
----------------------------------------------------- */
add_action('vc_before_init', 'vc_tagline_box');
function vc_tagline_box()
{
    vc_map(array(
        "name" => __("Tagline box", "integrita"),
        "base" => "tagline_box",
        "class" => "",
        "category" => __("Integrita", "integrita"),
        "icon" => "fa fa-camera-retro",
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Title", "integrita"),
                "param_name" => "title",
                "value" => "",
                "description" => __("Title", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Text", "integrita"),
                "param_name" => "content",
                "value" => "",
                "description" => __("Text", "integrita")
            ),
            array(
                "type" => "colorpicker",
                "holder" => "div",
                "class" => "",
                "heading" => __("Color", "integrita"),
                "param_name" => "color",
                "value" => "",
                "description" => __("Color", "integrita")
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Border position.", "integrita"),
                "param_name" => "border_position",
                "value" => array(
                    "Top" => "top",
                    "Left" => "left",
                    "Bottom" => "bottom",
                    "Right" => "right"
                ),
                "description" => __("Border position.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Positioned border width.", "integrita"),
                "param_name" => "border_position_width",
                "value" => "",
                "description" => __("Positioned border width.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("All border width.", "integrita"),
                "param_name" => "all_border_width",
                "value" => "",
                "description" => __("All border width.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Text on the button.", "integrita"),
                "param_name" => "button_text",
                "value" => "",
                "description" => __("Text on the button.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("URL (Link)", "integrita"),
                "param_name" => "button_link",
                "value" => "",
                "description" => __("URL (Link)", "integrita")
            )      
        )
    ));
}
/* -----------------------------------------------------
Popover
----------------------------------------------------- */
add_action('vc_before_init', 'vc_popover');
function vc_popover()
{
    vc_map(array(
        "name" => __("Popover", "integrita"),
        "base" => "popover",
        "class" => "",
        "category" => __("Integrita", "integrita"),
        "icon" => "fa fa-camera-retro",
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Title", "integrita"),
                "param_name" => "title",
                "value" => "",
                "description" => __("Title", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Text", "integrita"),
                "param_name" => "content",
                "value" => "",
                "description" => __("Text", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("URL (Link)", "integrita"),
                "param_name" => "button_link",
                "value" => "",
                "description" => __("URL (Link)", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Text on the button.", "integrita"),
                "param_name" => "button_text",
                "value" => "",
                "description" => __("Text on the button.", "integrita")
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Placement", "integrita"),
                "param_name" => "placement",
                "value" => array(
                    "Top" => "top",
                    "Left" => "left",
                    "Bottom" => "bottom",
                    "Right" => "right"
                ),
                "description" => __("Placement", "integrita")
            )        
        )
    ));
}
/* -----------------------------------------------------
Pricing table
----------------------------------------------------- */
add_action('vc_before_init', 'vc_pricing_table');
function vc_pricing_table()
{
    vc_map(array(
        "name" => __("Pricing table", "integrita"),
        "base" => "pricing_table",
        "class" => "",
        "category" => __("Integrita", "integrita"),
        "icon" => "fa fa-camera-retro",
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Title", "integrita"),
                "param_name" => "title",
                "value" => "",
                "description" => __("Title", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Text", "integrita"),
                "param_name" => "content",
                "value" => "",
                "description" => __("Example: [listitem]1[/listitem][listitem]2[/listitem][listitem]3[/listitem]", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Prefix", "integrita"),
                "param_name" => "prefix",
                "value" => "",
                "description" => __("Prefix", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Normal price.", "integrita"),
                "param_name" => "normalprice",
                "value" => "",
                "description" => __("Normal price.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Decimal price.", "integrita"),
                "param_name" => "decimalprice",
                "value" => "",
                "description" => __("Decimal price.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Postfix", "integrita"),
                "param_name" => "postfix",
                "value" => "monthly",
                "description" => __("Postfix", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("URL (Link)", "integrita"),
                "param_name" => "button_link",
                "value" => "",
                "description" => __("URL (Link)", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Text on the button.", "integrita"),
                "param_name" => "button_text",
                "value" => "",
                "description" => __("Text on the button.", "integrita")
            )        
        )
    ));
}
/* -----------------------------------------------------
Google map
----------------------------------------------------- */
add_action('vc_before_init', 'vc_google_map');
function vc_google_map()
{
    vc_map(array(
        "name" => __("Google map", "integrita"),
        "base" => "map",
        "class" => "",
        "category" => __("Integrita", "integrita"),
        "icon" => "fa fa-camera-retro",
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Position latitude.", "integrita"),
                "param_name" => "position_latitude",
                "value" => "",
                "description" => __("Position latitude.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Position longitude.", "integrita"),
                "param_name" => "position_longitude",
                "value" => "",
                "description" => __("Position longitude.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Marker latitude.", "integrita"),
                "param_name" => "marker_latitude",
                "value" => "",
                "description" => __("Marker latitude.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Marker longitude.", "integrita"),
                "param_name" => "marker_longitude",
                "value" => "",
                "description" => __("Marker longitude.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Title", "integrita"),
                "param_name" => "title",
                "value" => "",
                "description" => __("Title", "integrita")
            )      
        )
    ));
}
/* -----------------------------------------------------
Content box
----------------------------------------------------- */
add_action('vc_before_init', 'vc_content_box');
function vc_content_box()
{
    vc_map(array(
        "name" => __("Content box", "integrita"),
        "base" => "content_box",
        "class" => "",
        "category" => __("Integrita", "integrita"),
        "icon" => "fa fa-camera-retro",
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Text in content box.", "integrita"),
                "param_name" => "content",
                "value" => "",
                "description" => __("Text in content box.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("icon", "integrita"),
                "param_name" => "icon",
                "value" => "",
                "description" => __("icon", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Title", "integrita"),
                "param_name" => "title",
                "value" => "",
                "description" => __("Title", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("URL (Link)", "integrita"),
                "param_name" => "link",
                "value" => "",
                "description" => __("URL (Link)", "integrita")
            ),
            array(
                "type" => "colorpicker",
                "holder" => "div",
                "class" => "",
                "heading" => __("Background color.", "integrita"),
                "param_name" => "background",
                "value" => "",
                "description" => __("Background color.", "integrita")
            )     
        )
    ));
}
/* -----------------------------------------------------
Alert box
----------------------------------------------------- */
add_action('vc_before_init', 'vc_alert_box');
function vc_alert_box()
{
    vc_map(array(
        "name" => __("Alert box", "integrita"),
        "base" => "alert",
        "class" => "",
        "category" => __("Integrita", "integrita"),
        "icon" => "fa fa-camera-retro",
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Text in alert box.", "integrita"),
                "param_name" => "content",
                "value" => "",
                "description" => __("Text in alert box.", "integrita")
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Background color.", "integrita"),
                "param_name" => "color",
                "value" => array(
                    "Default" => "default",
                    "Primary" => "primary",
                    "Success" => "success",
                    "Info" => "info",
                    "Warning" => "warning",
                    "Danger" => "danger",
                    "Link" => "link"
                ),
                "description" => __("Background color.", "integrita")
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Dismissible switch.", "integrita"),
                "param_name" => "dismissible",
                "value" => array(
                    "Off" => "off",
                    "On" => "on"
                ),
                "description" => __("Dismissible switch.", "integrita")
            )
        )
    ));
}
/* -----------------------------------------------------
Heading
----------------------------------------------------- */
add_action('vc_before_init', 'vc_heading');
function vc_heading()
{
    vc_map(array(
        "name" => __("Heading", "integrita"),
        "base" => "heading",
        "class" => "",
        "category" => __("Integrita", "integrita"),
        "icon" => "fa fa-camera-retro",
        "params" => array(
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Tag", "integrita"),
                "param_name" => "tag",
                "value" => array(
                    "Heading 1" => "h1",
                    "Heading 2" => "h2",
                    "Heading 3" => "h3",
                    "Heading 4" => "h4",
                    "Heading 5" => "h5",
                    "Heading 6" => "h6"
                ),
                "description" => __("Tag", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Text", "integrita"),
                "param_name" => "text",
                "value" => "",
                "description" => __("Text", "integrita")
            )
        )
    ));
}
/* -----------------------------------------------------
Countup
----------------------------------------------------- */
add_action('vc_before_init', 'vc_countup');
function vc_countup()
{
    vc_map(array(
        "name" => __("Counter", "integrita"),
        "base" => "counter",
        "class" => "",
        "category" => __("Integrita", "integrita"),
        "icon" => "fa fa-camera-retro",
        "params" => array(
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Tag", "integrita"),
                "param_name" => "tag",
                "value" => array(
                    "Heading 1" => "h1",
                    "Heading 2" => "h2",
                    "Heading 3" => "h3",
                    "Heading 4" => "h4",
                    "Heading 5" => "h5",
                    "Heading 6" => "h6",
                    "Span" => "span"
                ),
                "description" => __("Tag", "integrita")
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Border", "integrita"),
                "param_name" => "border",
                "value" => array(
                    "Off" => "off",
                    "On" => "on"
                ),
                "description" => __("border", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Minimum number.", "integrita"),
                "param_name" => "min",
                "value" => "0",
                "description" => __("Minimum number.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Maximum number.", "integrita"),
                "param_name" => "max",
                "value" => "1000",
                "description" => __("Maximum number.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Timer delay.", "integrita"),
                "param_name" => "delay",
                "value" => "10",
                "description" => __("Text in alert box.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("A szám növelésének mértéke. (increment)", "integrita"),
                "param_name" => "increment",
                "value" => "9",
                "description" => __("A szám növelésének mértéke. (increment)", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Number prefix.", "integrita"),
                "param_name" => "prefix",
                "value" => "",
                "description" => __("Number prefix.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Number postfix.", "integrita"),
                "param_name" => "postfix",
                "value" => "",
                "description" => __("Number postfix.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Comment", "integrita"),
                "param_name" => "comment",
                "value" => "",
                "description" => __("Comment", "integrita")
            )
        )
    ));
}
/* -----------------------------------------------------
Knob
----------------------------------------------------- */
add_action('vc_before_init', 'vc_knob');
function vc_knob()
{
    vc_map(array(
        "name" => __("Knob", "integrita"),
        "base" => "knob",
        "class" => "",
        "category" => __("Integrita", "integrita"),
        "icon" => "fa fa-camera-retro",
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Value", "integrita"),
                "param_name" => "value",
                "value" => "1000",
                "description" => __("Value", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Thickness", "integrita"),
                "param_name" => "thickness",
                "value" => ".1",
                "description" => __("Thickness", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Title", "integrita"),
                "param_name" => "title",
                "value" => "",
                "description" => __("Title", "integrita")
            )
        )
    ));
}
/* -----------------------------------------------------
Person
----------------------------------------------------- */
add_action('vc_before_init', 'vc_person');
function vc_person()
{
    $team_names = array();
    $args = array( 'post_type' => 'team' );
    $loop = new WP_Query( $args );

    if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
        $original_title = get_the_title();
        $strtolow_title = str_replace(' ', '_', strtolower($original_title));;
        $team_names[$original_title] = $original_title;
    endwhile;
    else:
        $team_names = array( 'Elöször adj hozzá a csapatodhoz egy új tagot. A teams menüpontban.'=>'empty', );
    endif;

    vc_map(array(
        "name" => __("Team member", "integrita"),
        "base" => "team_member",
        "class" => "",
        "category" => __("Integrita", "integrita"),
        "icon" => "fa fa-camera-retro",
        "params" => array(
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Team name", "integrita"),
                "param_name" => "name",
                "value" => $team_names,
                "description" => __("Select please a member name.", "integrita")
            )           
        )
    ));
}
/* -----------------------------------------------------
Post types display
----------------------------------------------------- */
add_action('vc_before_init', 'vc_list_selected_post_types');
function vc_list_selected_post_types()
{ 
    vc_map(array(
        "name" => __("Post types display", "integrita"),
        "base" => "post_types_display",
        "class" => "",
        "category" => __("Integrita", "integrita"),
        "icon" => "fa fa-camera-retro",
        "params" => array(
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Kilistázuk a post typeokat.", "integrita"),
                "param_name" => "selected_post_type",
                "value" => array(
                    "Blog" => "post",
                    "Portfolio" => "portfolio",
                    "Team" => "team"
                ),
                "description" => __("Kilistázuk a post typeokat.", "integrita")
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Column numbers.", "integrita"),
                "param_name" => "column",
                "value" => array(
                    "One" => "one",
                    "Two" => "two",
                    "Three" => "three",
                    "Four" => "four",
                    "Six" => "six"
                ),
                "description" => __("Column numbers.", "integrita")
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Slider", "integrita"),
                "param_name" => "slider",
                "value" => array(
                    "None" => "none",
                    "Slick" => "slick"
                ),
                "description" => __("Slider", "integrita")
            )       
        )
    ));
}
/* -----------------------------------------------------
Divider
----------------------------------------------------- */
add_action('vc_before_init', 'vc_divider');
function vc_divider()
{ 
    vc_map(array(
        "name" => __("Divider", "integrita"),
        "base" => "divider",
        "class" => "",
        "category" => __("Integrita", "integrita"),
        "icon" => "fa fa-camera-retro",
        "params" => array(
            array(
                "type" => "colorpicker",
                "holder" => "div",
                "class" => "",
                "heading" => __("Border color.", "integrita"),
                "param_name" => "border_color",
                "value" => "#000000",
                "description" => __("Border color.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Margin top.", "integrita"),
                "param_name" => "margin_top",
                "value" => "16",
                "description" => __("Margin top.", "integrita")
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Margin bottom.", "integrita"),
                "param_name" => "margin_bottom",
                "value" => "16",
                "description" => __("Margin bottom.", "integrita")
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Border stlye", "integrita"),
                "param_name" => "border_styles",
                "value" => array(
                    "Hidden" => "hidden",
                    "Dotted" => "dotted",
                    "Dashed" => "dashed",
                    "Solid" => "solid",
                    "Souble" => "double",
                    "Groove" => "groove",
                    "Ridge" => "ridge",
                    "Inset" => "inset",
                    "Outset" => "outset"
                ),
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Border width.", "integrita"),
                "param_name" => "border_width",
                "value" => "1",
                "description" => __("Border width.", "integrita")
            )            
        )
    ));
}
/* -----------------------------------------------------
Progress bar
----------------------------------------------------- */
add_action('vc_before_init', 'vc_progress_bar');
function vc_progress_bar()
{ 
    vc_map(array(
        "name" => __("Progress bar", "integrita"),
        "base" => "progress",
        "class" => "",
        "category" => __("Integrita", "integrita"),
        "icon" => "fa fa-camera-retro",
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Value", "integrita"),
                "param_name" => "value",
                "value" => "50",
                "description" => __("0-100", "integrita")
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Color", "integrita"),
                "param_name" => "color",
                "value" => array(
                    "Default" => "default",
                    "Primary" => "primary",
                    "Success" => "success",
                    "Info" => "info",
                    "Warning" => "warning",
                    "Danger" => "danger",
                    "Link" => "link"
                ),
                "description" => __("Button color.", "integrita")
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Striped", "integrita"),
                "param_name" => "striped",
                "value" => array(
                    "Off" => "off",
                    "On" => "on"
                ),
                "description" => __("Striped", "integrita")
            ),
            array(
                "type" => "dropdown",
                "holder" => "div",
                "class" => "",
                "heading" => __("Animated", "integrita"),
                "param_name" => "animated",
                "value" => array(
                    "Off" => "off",
                    "On" => "on"
                ),
                "description" => __("Animated", "integrita")
            )
        )
    ));
}