<?php

if (!function_exists('int_bootstrap_button')) {
    function int_bootstrap_button($text, $url, $color, $size, $classes, $target, $block)
    {
        if ($size != '') {
            $size = ' btn-' . $size;
        }
        if ($classes != '') {
            $classes = ' ' . $classes;
        }
        if ($block == 'on') {
            $block = ' btn-block';
        } else {
            $block = '';
        }
        $output = '<a ' . 'target="' . $target . '" ' . 'href="' . $url . '">';
        $output .= '<button type="button" class="btn btn-' . $color . $size . $classes . $block . '">';
        $output .= $text;
        $output .= '</button>';
        $output .= '</a>';
        return $output;
    }
}

if (!function_exists('int_contex_colors')) {
    function int_contex_colors($text, $color)
    {
        $output = '<p class="text-' . $color . '">';
        $output .= $text;
        $output .= '</p>';
        return $output;
    }
}

if (!function_exists('int_jquery_knob')) {
    function int_jquery_knob($value, $thickness, $title)
    {
        $output = '<div class="counter-circle">';
            $output .= '<input type="text" class="dial" data-width="150" data-height="150" data-skin="tron" ' . 'value="' . $value . '" ' . 'data-thickness="' . $thickness . '">';
            $output .= '<p>' . $title . '</p>';
        $output .= '</div>';
        return $output;
    }
}

if (!function_exists('int_dismissible_alert')) {
    function int_dismissible_alert($text, $color, $dismissible)
    {
        if ($dismissible == 'on') {
            $dismissible = '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        } else {
            $dismissible = '';
        }
        $output = '<div class="alert alert-' . $color . ' alert-dismissible" role="alert">';
        $output .= $dismissible;
        $output .= $text;
        $output .= '</div>';
        return $output;
    }
}

if (!function_exists('int_blockquote')) {
    function int_blockquote($text, $reverse)
    {
        if ($reverse == 'on') {
            $reverse = 'blockquote-reverse';
        } else {
            $reverse = '';
        }
        
        $output = '<blockquote class="' . $reverse . '"><p>';
        $output .= $text;
        $output .= '</p></blockquote>';
        return $output;
    }
}

if (!function_exists('int_progress_bar')) {
    function int_progress_bar($value, $color, $align, $striped, $animated)
    {
        if ($striped == 'on') {
            $striped = ' progress-bar-striped';
        } else {
            $striped = '';
        }
        if ($animated == 'on') {
            $animated = ' active';
        } else {
            $animated = '';
        }
        $output = '<div class="progress ' . $align . '">';
        $output .= '<div class="progress-bar progress-bar-' . $color . $striped . $animated . '" role="progressbar" data-transitiongoal="' . $value . '" aria-valuemin="0" aria-valuemax="100">';
        $output .= $label_str;
        $output .= '</div>';
        $output .= '</div>';
        return $output;
    }
}

if (!function_exists('int_dropcap')) {
    function int_dropcap($text)
    {
        $output = '<div class="dropcap">' . $text . '</div>';
        return $output;
    }
}

if (!function_exists('int_social_icons')) {
    function int_social_icons($text, $class, $link, $target)
    {
        $block_or_not = ' btn-social';
        if (empty($text)) {
            $block_or_not = ' btn-social-icon';
        }
        $output = '<a href="' . $link . '" class="btn' . $block_or_not . ' btn-' . $class . '" target="' . $target . '">';
        $output .= '<i class="fa fa-' . $class . '"></i> ' . $text;
        $output .= '</a>';
        return $output;
    }
}

if (!function_exists('int_counter')) {
    function int_counter($tag, $min, $max, $delay, $increment, $prefix, $postfix, $comment, $border)
    {
        if ($tag == 'h1') {
            $tag = 'h1';
        } else if ($tag == 'h2') {
            $tag = 'h2';
        } else if ($tag == 'h3') {
            $tag = 'h3';
        } else if ($tag == 'h4') {
            $tag = 'h4';
        } else if ($tag == 'h5') {
            $tag = 'h5';
        } else if ($tag == 'h6') {
            $tag = 'h6';
        } else if ($tag == 'span') {
            $tag = 'span';
        } else {
            $tag = 'p';
        }
        $border_string = '';
        if($border == 'on'){ $border_string = ' counter-box-border'; }
        $output = '<div class="counter-box' . $border_string . '">';
        $output .= '<' . $tag . ' class="numscroller" data-slno="1" data-min="' . $min . '" data-max="' . $max . '" data-delay="' . $delay . '" data-increment="' . $increment . '" data-prefix="' . $prefix . '" data-postfix="' . $postfix . '">';
        $output .= '</' . $tag . '>';
        if(!empty($comment)) { $output .= '<p>' . $comment . '</p>'; }
        $output .= '</div>';
        return $output;
    }
}

if (!function_exists('int_google_map')) {
    function int_google_map($position_latitude, $position_longitude, $marker_latitude, $marker_longitude, $title)
    {
        $output = '<div class="gmap" data-position="' . $position_latitude . "," . $position_longitude . '" data-marker="' . $marker_latitude . "," . $marker_longitude . '" data-title="' . $title . '"></div>';
        return $output;
    }
}

if (!function_exists('int_heading')) {
    function int_heading($tag, $text) {
        $output = '<' . $tag . '>' . $text . '</' . $tag . '>';
        return $output;
    }
}

if (!function_exists('int_content_box')) {
    function int_content_box($text, $icon, $title, $link, $background)
    {
        $output = '<div class="content-box">';
        $output .= '<div class="content-box-head">';
        $output .= '<div class="cb-icon"><i class="fa fa-' . $icon . '"></i></div>';
        $output .= '</div>';
        $output .= '<div class="content-box-footer" data-background-color="' . $background . '">';
        $output .= '<div class="cb-title"><h1>' . $title . '</h1></div>';
        $output .= '<div class="cb-desc"><p>' . $text . '</p></div>';
        $output .= '<div class="cb-link"><a href="' . $link . '">Read more ...</a></div>';
        $output .= '</div>';
        $output .= '</div>';
        return $output;
    }
}

if (!function_exists('int_pricing_table')) {
    function int_pricing_table($content, $title, $prefix, $normalprice, $decimalprice, $postfix, $button_text, $button_link)
    {
        $output = '<div class="pricing-table">';
        $output .= '<div class="pricing-table-head">';
        $output .= '<h2>' . $title . '</h2>';
        $output .= '</div>';
        $output .= '<div class="pricing-table-price">';
        $output .= '<span class="prefix">' . $prefix . '</span>';
        $output .= '<span class="baseprice">' . $normalprice . '</span>';
        $output .= '<span class="decimalprice">' . $decimalprice . '</span>';
        $output .= '<span class="postfix">' . $postfix . '</span>';
        $output .= '</div>';
        $output .= '<div class="pricing-table-list">';
        $output .= '<ul class="list-unstyled no-margin">';
        $output .= do_shortcode($content);
        $output .= '</ul>';
        $output .= '</div>';
        $output .= '<div class="pricing-table-footer">';
        $output .= do_shortcode('[button link="' . $button_link . '"]' . $button_text . '[/button]');
        $output .= '</div>';
        $output .= '</div>';
        return $output;
    }
}

if (!function_exists('int_team_member')) {
    function int_team_member($name)
    {
        $args = array( 'post_type' => 'team', 'post_title_like' => $name );
        $loop = new WP_Query( $args );
        $output = '';
        if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
            if (has_post_thumbnail()) : 
                $thumbnail_link = get_permalink();
                $thumbnail = get_the_post_thumbnail($post->ID,'grid-portfolio-four-column');
                $team_position_value = get_post_meta( get_the_ID(), 'team-position-meta-box-value', true );                
                $facebook_url_value = get_post_meta( get_the_ID(), 'facebook-url-meta-box-value', true );
                $twitter_url_value = get_post_meta( get_the_ID(), 'twitter-url-meta-box-value', true );
                $soundcloud_url_value = get_post_meta( get_the_ID(), 'soundcloud-url-meta-box-value', true );
                $vimeo_url_value = get_post_meta( get_the_ID(), 'vimeo-url-meta-box-value', true );             
                $flickr_url_value = get_post_meta( get_the_ID(), 'flickr-url-meta-box-value', true );
                $linkedin_url_value = get_post_meta( get_the_ID(), 'linkedin-url-meta-box-value', true );
                $google_url_value = get_post_meta( get_the_ID(), 'google-url-meta-box-value', true );
                /* Output ******/
                $output = '<div class="team-member">';
                    $output .= '<div class="team-thumb">';
                        $output .= '<a href="' . $thumbnail_link . '">';
                        $output .= $thumbnail;
                        $output .= '</a>';
                    $output .= '</div>';
                    $output .= '<h3>' . get_the_title() . '</h3>';
                    if( !empty( $team_position_value ) ) { $output .= '<p>' . $team_position_value . '</p>'; }
                    $output .= '<p class="team-excerpt">' . get_the_excerpt() . '</p>';
                    $output .= '<div class="team-social">';                        
                        if( !empty( $facebook_url_value ) ) { $output .= '<a href="' . $facebook_url_value . '" class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook"></i></a>'; }                 
                        if( !empty( $twitter_url_value ) ) { $output .= '<a href="' . $twitter_url_value . '" class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></a>'; }
                        if( !empty( $soundcloud_url_value ) ) { $output .= '<a href="' . $soundcloud_url_value . '" class="btn btn-social-icon btn-soundcloud"><i class="fa fa-soundcloud"></i></a>'; }
                        if( !empty( $vimeo_url_value ) ) { $output .= '<a href="' . $vimeo_url_value . '" class="btn btn-social-icon btn-vimeo"><i class="fa fa-vimeo-square"></i></a>'; }
                        if( !empty( $flickr_url_value ) ) { $output .= '<a href="' . $flickr_url_value . '" class="btn btn-social-icon btn-flickr"><i class="fa fa-flickr"></i></a>'; }
                        if( !empty( $linkedin_url_value ) ) { $output .= '<a href="' . $linkedin_url_value . '" class="btn btn-social-icon btn-linkedin"><i class="fa fa-linkedin"></i></a>'; }
                        if( !empty( $google_url_value ) ) { $output .= '<a href="' . $google_url_value . '" class="btn btn-social-icon btn-google-plus"><i class="fa fa-google-plus"></i></a>'; }
                    $output .= '</div>';
                $output .= '</div>';
            endif;
        endwhile; 
        wp_reset_postdata();         
        endif;

        return $output;
    }
}

if (!function_exists('int_list_selected_post_types')) {
    function int_list_selected_post_types($selected_post_type, $column, $slider)
    {        
        if(empty($selected_post_type)) { $selected_post_type = 'post'; }
        $output = '';
        $column_count = 1;
        switch ($column):
            case "one":
                $column_count = 1;
                break;
            case "two":
                $column_count = 2;
                break;
            case "three":
                $column_count = 3;
                break;
            case "four":
                $column_count = 4;
                break;
            case "six":
                $column_count = 6;
                break; 
        endswitch;

        $loop = '';
        if($slider == 'none'):
        $none_args = array( 'post_type' => $selected_post_type, 'post_status' => 'publish', 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => $column_count);
        $loop = new WP_Query( $none_args );
        elseif($slider == 'slick'):
        $slick_args = array( 'post_type' => $selected_post_type, 'post_status' => 'publish', 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1);
        $loop = new WP_Query( $slick_args );
        endif;        

        if ($loop->have_posts()) : 
            if($slider == 'none'):

            $output = '<div class="row">';
                while ($loop->have_posts()) : $loop->the_post();
                $grid_system_column_count = 12/$column_count;
                $output .= '<div class="col-md-' . $grid_system_column_count . '">';

                    if (has_post_thumbnail()) :
                    $thumbnail_size = 'selected-post-types-' . $column . '-column';
                    $thumbnail = get_the_post_thumbnail($post->ID,$thumbnail_size);
                    $output .= '<div class="selected-post-type-thumbnail int-image-hover-parent">';
                    $output .= '<a href="' . get_permalink() . '">';
                    $output .= $thumbnail;
                    $output .= '</a>';
                    $output .= '<div class="int-image-hover">';
                        $output .= '<a href="' . get_permalink() . '"><i class="fa fa-search-plus fa-5"></i></a>';
                    $output .= '</div>';
                    $output .= '</div>';
                    endif;

                    $output .= '<h3>' . get_the_title() . '</h3>';
                    $output .= '<h4>' . get_the_date() . '</h4>';                
                    $output .= '<p>' . get_the_excerpt() . '</p>';

                $output .= '</div>';
                endwhile;
                wp_reset_postdata();                
            $output .= '</div>';

            elseif($slider == 'slick'):            

            $output = '<div class="row"><div class="slick-carousel" data-column-count="'.$column_count.'">';
                while ($loop->have_posts()) : $loop->the_post();

                $output .= '<div>';
                    if (has_post_thumbnail()) :
                    $thumbnail_size = 'selected-post-types-' . $column . '-column';
                    $thumbnail = get_the_post_thumbnail($post->ID,$thumbnail_size);

                    $output .= '<div class="selected-post-type-thumbnail int-image-hover-parent">';
                    $output .= '<a href="' . get_permalink() . '">';
                    $output .= $thumbnail;
                    $output .= '</a>';
                    $output .= '<div class="int-image-hover">';
                        $output .= '<a href="' . get_permalink() . '"><i class="fa fa-search-plus fa-5"></i></a>';
                    $output .= '</div>';
                    $output .= '</div>';

                    $output .= '<h3>' . get_the_title() . '</h3>';
                    $output .= '<h4>' . get_the_date() . '</h4>';
                    endif;
                $output .= '</div>';

                endwhile;
                wp_reset_postdata();
            $output .= '</div></div>';

            else:
                $output = '';
            endif;
        endif;

        return $output;
    }
}

if (!function_exists('int_youtube')) {
    function int_youtube($video_id, $width, $height)
    {
        $output = '<iframe width="' . $width . '" height="' . $height . '" src="//www.youtube.com/embed/' . $video_id . '" frameborder="0" allowfullscreen></iframe>';
        return $output;
    }
}

if (!function_exists('int_vimeo')) {
    function int_vimeo($video_id, $width, $height)
    {
        $output = '<iframe src="//player.vimeo.com/video/' . $video_id . '" width="' . $width . '" height="' . $height . '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
        return $output;
    }
}



if (!function_exists('int_tagline_box')) {
    function int_tagline_box($content, $title, $color, $border_position, $border_position_width, $all_border_width, $button_link, $button_text)
    {
        $positioned_border = '';
        $button            = '';
        if(empty($all_border_width)) { $all_border_width = '1'; }
        if ($border_position_width != '0') {
            if ($border_position == 'top') {
                $positioned_border = 'border-top-width:' . $border_position_width . 'px';
            } else if ($border_position == 'left') {
                $positioned_border = 'border-left-width:' . $border_position_width . 'px';
            } else if ($border_position == 'bottom') {
                $positioned_border = 'border-bottom-width:' . $border_position_width . 'px';
            } else if ($border_position == 'right') {
                $positioned_border = 'border-right-width:' . $border_position_width . 'px';
            }
        }
        if ($title != '') {
            $title = '<h2>' . $title . '</h2>';
        }
        if ($button_link != '' and $button_text != '') {
            $button = do_shortcode('[button link="' . $button_link . '"]' . $button_text . '[/button]');
        }
        $output = '<div class="tagline-box" style="border-color:' . $color . ';border-width:' . $all_border_width . 'px;' . $positioned_border . '">';
        $output .= $title;
        $output .= '<p>' . $content . '</p>';
        $output .= $button;
        $output .= '</div>';
        return $output;
    }
}

if (!function_exists('int_popover')) {
    function int_popover($content, $title, $button_link, $button_text, $placement)
    {
        if ($title == '') {
            $title = $button_text;
        }
        $output = '<a href="' . $button_link . '" tabindex="0" class="btn btn-lg btn-danger" role="button" data-placement="' . $placement . '" data-toggle="popover" data-trigger="focus" title="' . $title . '" data-content="' . $content . '">' . $button_text . '</a>';
        return $output;
    }
}

if (!function_exists('int_countup')) {
    function int_countup($min, $max, $delay, $increment)
    {
        $output = '<h1 class="numscroller" data-slno="1" data-min="' . $min . '" data-max="' . $max . '" data-delay="' . $delay . '" data-increment="' . $increment . '">0</h1>';
        return $output;
    }
}

if (!function_exists('int_divider')) {
    function int_divider($border_color, $margin_top, $margin_bottom, $border_width, $border_styles)
    {
        $output = '<div class="integrita-separator" style="border-color:'.$border_color.';margin-top:'.$margin_top.'px;margin-bottom:'.$margin_bottom.'px;border-width:'.$border_width.'px;border-style:'.$border_styles.';"></div>';
        return $output;
    }
}
/* -----------------------------------------------------
Temporary
----------------------------------------------------- */
if (!function_exists('int_list_item')) {
    function int_list_item($text)
    {
        $output = '<li>' . $text . '</li>';
        return $output;
    }
}

