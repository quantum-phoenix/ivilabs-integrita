<?php
/* -----------------------------------------------------
Portfolio post type
----------------------------------------------------- */
add_action('init', 'create_portfolio_post_type');
function create_portfolio_post_type()
{
    register_post_type('portfolio', array(
        'labels' => array(
            'name' => __('Portfolio'),
            'singular_name' => __('Portfolio item')
        ),
        'public' => true,
        'has_archive' => true,
        'menu_icon' => admin_url() . 'images/media-button-image.gif',
        'supports' => array(
          'title',
          'editor'          
        )
    ));
}
/* -----------------------------------------------------
Portfolio taxonomy
----------------------------------------------------- */
add_action('init', 'create_portfolio_taxonomys', 0);
function create_portfolio_taxonomys()
{
    register_taxonomy('portfolio_taxonomy', 'portfolio', array(
        'labels' => array(
            'name' => 'Portfolio Categories',
            'add_new_item' => 'Add New Portfolio Categorie',
            'new_item_name' => "Add New Portfolio Categorie Name"
        ),
        'show_ui' => true,
        'show_tagcloud' => true,
        'hierarchical' => true
    ));
}

/* -----------------------------------------------------
Team post type
----------------------------------------------------- */
add_action('init', 'create_team_post_type');
function create_team_post_type()
{
    register_post_type('team', array(
        'labels' => array(
            'name' => __('Team'),
            'singular_name' => __('Team member')
        ),
        'public' => true,
        'has_archive' => true,
        'supports' => array(
          'title',
          'editor',
          'thumbnail'
        )
    ));
}