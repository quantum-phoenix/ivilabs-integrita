<?php
/* -----------------------------------------------------
Singe Template Changer Meta box
----------------------------------------------------- */
add_action( 'add_meta_boxes', 'portfolio_single_layout_meta_boxes' );
function portfolio_single_layout_meta_boxes() {
    add_meta_box( 'portfolio_single_layout_meta_box', 'Hozzé', 'portfolio_single_layout_meta_box_handler', 'portfolio' );
}
function portfolio_single_layout_meta_box_handler($post) {
    $value = get_post_meta( $post->ID, 'portfolio_single_layout_meta_box', true );
    echo '<label for="portfolio_single_layout_meta_box">';
        _e( 'Portfolio single layout:', 'integrita' );
    echo '</label> ';
    $meta_element_selected = get_post_meta($post->ID, 'portfolio_single_layout_meta_box', true);
    echo $meta_element_selected;
    ?> 
    <select name="portfolio_single_layout_meta_box" id="portfolio_single_layout_meta_box">
      <option value="normal_full" <?php selected( $meta_element_selected, 'normal_full' ); ?>>Normal full layout</option>
      <option value="normal_left_sidebar" <?php selected( $meta_element_selected, 'normal_left_sidebar' ); ?>>Normal left sidebar layout</option>
      <option value="normal_right_sidebar" <?php selected( $meta_element_selected, 'normal_right_sidebar' ); ?>>Normal right sidebar layout</option>
      <option value="normal_full_paged" <?php selected( $meta_element_selected, 'normal_full_paged' ); ?>>Normal full paged</option>
    </select>
    <?php
}
add_action( 'save_post', 'portfolio_single_layout_meta_box_save' );
function portfolio_single_layout_meta_box_save($post_id) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }
    if(!current_user_can('edit_post')){
        return;
    }
    if ( ! isset( $_POST['portfolio_single_layout_meta_box'] ) ) {
        return;
    }
    $clean_data = sanitize_text_field( $_POST['portfolio_single_layout_meta_box'] );
    update_post_meta( $post_id , 'portfolio_single_layout_meta_box', $clean_data );
}
/* -----------------------------------------------------
Multi Image Meta box
----------------------------------------------------- */

add_action( 'admin_init', 'portfolio_multi_image_meta_box' );
add_action( 'admin_head-post.php', 'portfolio_multi_image_print_scripts' );
add_action( 'admin_head-post-new.php', 'portfolio_multi_image_print_scripts' );
add_action( 'save_post', 'portfolio_multi_image_meta_box_save', 10, 2 );

/**
 * Add custom Meta Box to Posts post type
 */
function portfolio_multi_image_meta_box() 
{
    add_meta_box(
        'portfolio_multi_image',
        'Studio Image Uploader',
        'portfolio_multi_image_meta_box_handler',
        'portfolio'
    );
}

/**
 * Print the Meta Box content
 */
function portfolio_multi_image_meta_box_handler($post) 
{
    $gallery_data = get_post_meta( $post->ID, 'portfolio_multi_image_data', true );
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'noncename_so_14445904' );
?>

<div id="dynamic_form">

    <div id="field_wrap">
    <?php 
    if ( isset( $gallery_data['image_url'] ) ) 
    {
        for( $i = 0; $i < count( $gallery_data['image_url'] ); $i++ ) 
        {
        ?>

        <div class="field_row">

          <div class="field_left">
            <div class="form_field">
              <label>Image URL</label>
              <input type="text"
                     class="meta_image_url"
                     name="gallery[image_url][]"
                     value="<?php esc_html_e( $gallery_data['image_url'][$i] ); ?>"
              />
            </div>
          </div>

          <div class="field_right image_wrap">
            <img src="<?php esc_html_e( $gallery_data['image_url'][$i] ); ?>" height="48" width="48" />
          </div>

          <div class="field_right">
            <input class="button" type="button" value="Choose File" onclick="add_image(this)" /><br />
            <input class="button" type="button" value="Remove" onclick="remove_field(this)" />
          </div>

          <div class="clear" /></div> 
        </div>
        <?php
        } // endif
    } // endforeach
    ?>
    </div>

    <div style="display:none" id="master-row">
    <div class="field_row">
        <div class="field_left">
            <div class="form_field">
                <label>Image URL</label>
                <input class="meta_image_url" value="" type="text" name="gallery[image_url][]" />
            </div>
        </div>
        <div class="field_right image_wrap">
        </div> 
        <div class="field_right"> 
            <input type="button" class="button" value="Choose File" onclick="add_image(this)" />
            <br />
            <input class="button" type="button" value="Remove" onclick="remove_field(this)" /> 
        </div>
        <div class="clear"></div>
    </div>
    </div>

    <div id="add_field_row">
      <input class="button" type="button" value="Add Field" onclick="add_field_row();" />
    </div>

</div>

  <?php
}

/**
 * Print styles and scripts
 */
function portfolio_multi_image_print_scripts()
{
    global $post;
    // Check for correct post_type
    if( 'portfolio' != $post->post_type )// here you can set post type name
        return;
    ?>  
    <style type="text/css">
      .field_left {
        float:left;
      }

      .field_right {
        float:left;
        margin-left:10px;
      }

      .clear {
        clear:both;
      }

      #dynamic_form {
        width:580px;
      }

      #dynamic_form input[type=text] {
        width:300px;
      }

      #dynamic_form .field_row {
        border:1px solid #999;
        margin-bottom:10px;
        padding:10px;
      }

      #dynamic_form label {
        padding:0 6px;
      }
    </style>

    <script type="text/javascript">
        function add_image(obj) {
            var parent=jQuery(obj).parent().parent('div.field_row');
            var inputField = jQuery(parent).find("input.meta_image_url");

            tb_show('', 'media-upload.php?TB_iframe=true');

            window.send_to_editor = function(html) {
                var url = jQuery(html).find('img').attr('src');
                inputField.val(url);
                jQuery(parent)
                .find("div.image_wrap")
                .html('<img src="'+url+'" height="48" width="48" />');

                // inputField.closest('p').prev('.awdMetaImage').html('<img height=120 width=120 src="'+url+'"/><p>URL: '+ url + '</p>'); 

                tb_remove();
            };

            return false;  
        }

        function remove_field(obj) {
            var parent=jQuery(obj).parent().parent();
            //console.log(parent)
            parent.remove();
        }

        function add_field_row() {
            var row = jQuery('#master-row').html();
            jQuery(row).appendTo('#field_wrap');
        }
    </script>
    <?php
}

/**
 * Save post action, process fields
 */
function portfolio_multi_image_meta_box_save( $post_id, $post_object ) 
{
    // Doing revision, exit earlier **can be removed**
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )  
        return;

    // Doing revision, exit earlier
    if ( 'revision' == $post_object->post_type )
        return;

    // Correct post type
    if ( $_POST['post_type'] != 'portfolio' ) { // here you can set post type name
        return;
    }

    // Verify authenticity
    if ( !wp_verify_nonce( $_POST['noncename_so_14445904'], plugin_basename( __FILE__ ) ) )
        return;

    if ( $_POST['gallery'] ) 
    {
        // Build array for saving post meta
        $gallery_data = array();
        for ($i = 0; $i < count( $_POST['gallery']['image_url'] ); $i++ ) 
        {
            if ( '' != $_POST['gallery']['image_url'][ $i ] ) 
            {
                $gallery_data['image_url'][]  = $_POST['gallery']['image_url'][ $i ];
            }
        }

        if ( $gallery_data ) 
            update_post_meta( $post_id, 'portfolio_multi_image_data', $gallery_data );
        else 
            delete_post_meta( $post_id, 'portfolio_multi_image_data' );
    } 
    // Nothing received, all fields are empty, delete option
    else 
    {
        delete_post_meta( $post_id, 'portfolio_multi_image_data' );
    }
}

/* -----------------------------------------------------
Team metaboxes
----------------------------------------------------- */

/* Team position ******/
add_action( 'add_meta_boxes', 'team_position_meta_box_add' );
function team_position_meta_box_add() {
    add_meta_box( 'team_position_meta_box', __( 'Team member position', 'integrita' ), 'team_position_meta_box_callback', 'team' );
}

function team_position_meta_box_callback( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'team_position_nonce' );
    $team_position_stored_meta = get_post_meta( $post->ID );
    ?>
    <label for="team-position-meta-box-value"><?php _e( 'Example Text Input', 'integrita' )?></label>
    <input type="text" name="team-position-meta-box-value" id="team-position-meta-box-value" value="<?php if ( isset ( $team_position_stored_meta['team-position-meta-box-value'] ) ) echo $team_position_stored_meta['team-position-meta-box-value'][0]; ?>" />
    <?php  
}

add_action( 'save_post', 'team_position_meta_box_save' );
function team_position_meta_box_save( $post_id ) {
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'team_position_nonce' ] ) && wp_verify_nonce( $_POST[ 'team_position_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
     // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce || $_POST['post_type'] != 'team') {
        return;
    }
    // Checks for input and sanitizes/saves if needed
    if( isset( $_POST[ 'team-position-meta-box-value' ] ) ) {
        update_post_meta( $post_id, 'team-position-meta-box-value', sanitize_text_field( $_POST[ 'team-position-meta-box-value' ] ) );
    } 
}

/* Facebook URL ******/
add_action( 'add_meta_boxes', 'facebook_url_meta_box_add' );
function facebook_url_meta_box_add() {
    add_meta_box( 'facebook_url_meta_box', __( 'Facebook URL', 'integrita' ), 'facebook_url_meta_box_callback', 'team' );
}

function facebook_url_meta_box_callback( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'facebook_url_nonce' );
    $facebook_url_stored_meta = get_post_meta( $post->ID );
    ?>
    <label for="facebook-url-meta-box-value"><?php _e( 'Facebook URL', 'integrita' )?></label>
    <input type="text" name="facebook-url-meta-box-value" id="facebook-url-meta-box-value" value="<?php if ( isset ( $facebook_url_stored_meta['facebook-url-meta-box-value'] ) ) echo $facebook_url_stored_meta['facebook-url-meta-box-value'][0]; ?>" />
    <?php  
}

add_action( 'save_post', 'facebook_url_meta_box_save' );
function facebook_url_meta_box_save( $post_id ) {
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'facebook_url_nonce' ] ) && wp_verify_nonce( $_POST[ 'facebook_url_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
     // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce || $_POST['post_type'] != 'team') {
        return;
    }
    // Checks for input and sanitizes/saves if needed
    if( isset( $_POST[ 'facebook-url-meta-box-value' ] ) ) {
        update_post_meta( $post_id, 'facebook-url-meta-box-value', sanitize_text_field( $_POST[ 'facebook-url-meta-box-value' ] ) );
    } 
}

/* Twitter URL ******/
add_action( 'add_meta_boxes', 'twitter_url_meta_box_add' );
function twitter_url_meta_box_add() {
    add_meta_box( 'twitter_url_meta_box', __( 'Twitter URL', 'integrita' ), 'twitter_url_meta_box_callback', 'team' );
}

function twitter_url_meta_box_callback( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'twitter_url_nonce' );
    $twitter_url_stored_meta = get_post_meta( $post->ID );
    ?>
    <label for="twitter-url-meta-box-value"><?php _e( 'Twitter URL', 'integrita' )?></label>
    <input type="text" name="twitter-url-meta-box-value" id="twitter-url-meta-box-value" value="<?php if ( isset ( $twitter_url_stored_meta['twitter-url-meta-box-value'] ) ) echo $twitter_url_stored_meta['twitter-url-meta-box-value'][0]; ?>" />
    <?php  
}

add_action( 'save_post', 'twitter_url_meta_box_save' );
function twitter_url_meta_box_save( $post_id ) {
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'twitter_url_nonce' ] ) && wp_verify_nonce( $_POST[ 'twitter_url_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
     // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce || $_POST['post_type'] != 'team') {
        return;
    }
    // Checks for input and sanitizes/saves if needed
    if( isset( $_POST[ 'twitter-url-meta-box-value' ] ) ) {
        update_post_meta( $post_id, 'twitter-url-meta-box-value', sanitize_text_field( $_POST[ 'twitter-url-meta-box-value' ] ) );
    } 
}

/* Soundcloud URL ******/
add_action( 'add_meta_boxes', 'soundcloud_url_meta_box_add' );
function soundcloud_url_meta_box_add() {
    add_meta_box( 'soundcloud_url_meta_box', __( 'Soundcloud URL', 'integrita' ), 'soundcloud_url_meta_box_callback', 'team' );
}

function soundcloud_url_meta_box_callback( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'soundcloud_url_nonce' );
    $soundcloud_url_stored_meta = get_post_meta( $post->ID );
    ?>
    <label for="soundcloud-url-meta-box-value"><?php _e( 'Soundcloud URL', 'integrita' )?></label>
    <input type="text" name="soundcloud-url-meta-box-value" id="soundcloud-url-meta-box-value" value="<?php if ( isset ( $soundcloud_url_stored_meta['soundcloud-url-meta-box-value'] ) ) echo $soundcloud_url_stored_meta['soundcloud-url-meta-box-value'][0]; ?>" />
    <?php  
}

add_action( 'save_post', 'soundcloud_url_meta_box_save' );
function soundcloud_url_meta_box_save( $post_id ) {
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'soundcloud_url_nonce' ] ) && wp_verify_nonce( $_POST[ 'soundcloud_url_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
     // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce || $_POST['post_type'] != 'team') {
        return;
    }
    // Checks for input and sanitizes/saves if needed
    if( isset( $_POST[ 'soundcloud-url-meta-box-value' ] ) ) {
        update_post_meta( $post_id, 'soundcloud-url-meta-box-value', sanitize_text_field( $_POST[ 'soundcloud-url-meta-box-value' ] ) );
    } 
}

/* Vimeo URL ******/
add_action( 'add_meta_boxes', 'vimeo_url_meta_box_add' );
function vimeo_url_meta_box_add() {
    add_meta_box( 'vimeo_url_meta_box', __( 'Vimeo URL', 'integrita' ), 'vimeo_url_meta_box_callback', 'team' );
}

function vimeo_url_meta_box_callback( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'vimeo_url_nonce' );
    $vimeo_url_stored_meta = get_post_meta( $post->ID );
    ?>
    <label for="vimeo-url-meta-box-value"><?php _e( 'Vimeo URL', 'integrita' )?></label>
    <input type="text" name="vimeo-url-meta-box-value" id="vimeo-url-meta-box-value" value="<?php if ( isset ( $vimeo_url_stored_meta['vimeo-url-meta-box-value'] ) ) echo $vimeo_url_stored_meta['vimeo-url-meta-box-value'][0]; ?>" />
    <?php  
}

add_action( 'save_post', 'vimeo_url_meta_box_save' );
function vimeo_url_meta_box_save( $post_id ) {
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'vimeo_url_nonce' ] ) && wp_verify_nonce( $_POST[ 'vimeo_url_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
     // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce || $_POST['post_type'] != 'team') {
        return;
    }
    // Checks for input and sanitizes/saves if needed
    if( isset( $_POST[ 'vimeo-url-meta-box-value' ] ) ) {
        update_post_meta( $post_id, 'vimeo-url-meta-box-value', sanitize_text_field( $_POST[ 'vimeo-url-meta-box-value' ] ) );
    } 
}

/* Flickr URL ******/
add_action( 'add_meta_boxes', 'flickr_url_meta_box_add' );
function flickr_url_meta_box_add() {
    add_meta_box( 'flickr_url_meta_box', __( 'Flickr URL', 'integrita' ), 'flickr_url_meta_box_callback', 'team' );
}

function flickr_url_meta_box_callback( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'flickr_url_nonce' );
    $flickr_url_stored_meta = get_post_meta( $post->ID );
    ?>
    <label for="flickr-url-meta-box-value"><?php _e( 'Flickr URL', 'integrita' )?></label>
    <input type="text" name="flickr-url-meta-box-value" id="flickr-url-meta-box-value" value="<?php if ( isset ( $flickr_url_stored_meta['flickr-url-meta-box-value'] ) ) echo $flickr_url_stored_meta['flickr-url-meta-box-value'][0]; ?>" />
    <?php  
}

add_action( 'save_post', 'flickr_url_meta_box_save' );
function flickr_url_meta_box_save( $post_id ) {
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'flickr_url_nonce' ] ) && wp_verify_nonce( $_POST[ 'flickr_url_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
     // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce || $_POST['post_type'] != 'team') {
        return;
    }
    // Checks for input and sanitizes/saves if needed
    if( isset( $_POST[ 'flickr-url-meta-box-value' ] ) ) {
        update_post_meta( $post_id, 'flickr-url-meta-box-value', sanitize_text_field( $_POST[ 'flickr-url-meta-box-value' ] ) );
    } 
}

/* Linkedin URL ******/
add_action( 'add_meta_boxes', 'linkedin_url_meta_box_add' );
function linkedin_url_meta_box_add() {
    add_meta_box( 'linkedin_url_meta_box', __( 'Linkedin URL', 'integrita' ), 'linkedin_url_meta_box_callback', 'team' );
}

function linkedin_url_meta_box_callback( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'linkedin_url_nonce' );
    $linkedin_url_stored_meta = get_post_meta( $post->ID );
    ?>
    <label for="linkedin-url-meta-box-value"><?php _e( 'Linkedin URL', 'integrita' )?></label>
    <input type="text" name="linkedin-url-meta-box-value" id="linkedin-url-meta-box-value" value="<?php if ( isset ( $linkedin_url_stored_meta['linkedin-url-meta-box-value'] ) ) echo $linkedin_url_stored_meta['linkedin-url-meta-box-value'][0]; ?>" />
    <?php  
}

add_action( 'save_post', 'linkedin_url_meta_box_save' );
function linkedin_url_meta_box_save( $post_id ) {
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'linkedin_url_nonce' ] ) && wp_verify_nonce( $_POST[ 'linkedin_url_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
     // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce || $_POST['post_type'] != 'team') {
        return;
    }
    // Checks for input and sanitizes/saves if needed
    if( isset( $_POST[ 'linkedin-url-meta-box-value' ] ) ) {
        update_post_meta( $post_id, 'linkedin-url-meta-box-value', sanitize_text_field( $_POST[ 'linkedin-url-meta-box-value' ] ) );
    } 
}

/* Google URL ******/
add_action( 'add_meta_boxes', 'google_url_meta_box_add' );
function google_url_meta_box_add() {
    add_meta_box( 'google_url_meta_box', __( 'Google URL', 'integrita' ), 'google_url_meta_box_callback', 'team' );
}

function google_url_meta_box_callback( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'google_url_nonce' );
    $google_url_stored_meta = get_post_meta( $post->ID );
    ?>
    <label for="google-url-meta-box-value"><?php _e( 'Google URL', 'integrita' )?></label>
    <input type="text" name="google-url-meta-box-value" id="google-url-meta-box-value" value="<?php if ( isset ( $google_url_stored_meta['google-url-meta-box-value'] ) ) echo $google_url_stored_meta['google-url-meta-box-value'][0]; ?>" />
    <?php  
}

add_action( 'save_post', 'google_url_meta_box_save' );
function google_url_meta_box_save( $post_id ) {
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'google_url_nonce' ] ) && wp_verify_nonce( $_POST[ 'google_url_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
     // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce || $_POST['post_type'] != 'team') {
        return;
    }
    // Checks for input and sanitizes/saves if needed
    if( isset( $_POST[ 'google-url-meta-box-value' ] ) ) {
        update_post_meta( $post_id, 'google-url-meta-box-value', sanitize_text_field( $_POST[ 'google-url-meta-box-value' ] ) );
    } 
}

/* -----------------------------------------------------
Post format metaboxes
----------------------------------------------------- */
/* Embed video ******/
add_action( 'add_meta_boxes', 'video_post_format_embed_meta_box_add' );
function video_post_format_embed_meta_box_add() {
    add_meta_box( 'video_post_format_embed_meta_box', __( 'Youtube video id for video post format', 'integrita' ), 'video_post_format_embed_meta_box_callback', 'post' );
}

function video_post_format_embed_meta_box_callback( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'video_post_format_embed_nonce' );
    $video_post_format_embed_meta_box = get_post_meta( $post->ID );
    ?>
    <input type="text" name="video_post_format_embed_value" id="video_post_format_embed_value" value="<?php if ( isset ( $video_post_format_embed_meta_box['video_post_format_embed_value'] ) ) echo $video_post_format_embed_meta_box['video_post_format_embed_value'][0]; ?>" />
    <?php  
}

add_action( 'save_post', 'video_post_format_embed_meta_box_save' );
function video_post_format_embed_meta_box_save( $post_id ) {
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'video_post_format_embed_nonce' ] ) && wp_verify_nonce( $_POST[ 'video_post_format_embed_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
     // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce || $_POST['post_type'] != 'post') {
        return;
    }
    // Checks for input and sanitizes/saves if needed
    if( isset( $_POST[ 'video_post_format_embed_value' ] ) ) {
        update_post_meta( $post_id, 'video_post_format_embed_value', sanitize_text_field( $_POST[ 'video_post_format_embed_value' ] ) );
    } 
}