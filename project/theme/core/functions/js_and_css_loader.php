<?php
/* -----------------------------------------------------
Frontend javascripts and css loader
----------------------------------------------------- */
function integrita_register_scripts()
{
    if (!is_admin()) {
        /*-------------- Css --------------*/
        wp_register_style(
                'googleapis_css',
                'http://fonts.googleapis.com/css?family=Droid+Sans:400,700'
        );
        wp_enqueue_style('googleapis_css');
        /*----------------------------*/
        wp_register_style(
            'bootstrap',
            get_bloginfo('stylesheet_directory') . '/core/css/bootstrap.min.css'
        );
        wp_enqueue_style('bootstrap');
        /*----------------------------*/
        wp_register_style(
            'bootstrap_theme',
            get_bloginfo('stylesheet_directory') . '/core/css/bootstrap-theme.min.css'
        );
        wp_enqueue_style('bootstrap_theme');
        /*----------------------------*/
        wp_register_style(
            'bootstrap_social',
            get_bloginfo('stylesheet_directory') . '/core/css/bootstrap-social.css'
        );
        wp_enqueue_style('bootstrap_social');
        /*----------------------------*/
        wp_register_style(
            'font_awesome_css',
            get_bloginfo('stylesheet_directory') . '/core/css/font-awesome.min.css'
        );
        wp_enqueue_style('font_awesome_css');       
        /*----------------------------*/
        wp_register_style(
            'animate_css',
            get_bloginfo('stylesheet_directory') . '/core/css/animate.css'
        );
        wp_enqueue_style('animate_css');       
        /*----------------------------*/        
        wp_register_style(
            'font_awesome',
            get_bloginfo('stylesheet_directory') . '/core/css/font-awesome.min.css'
        );
        wp_enqueue_style('font_awesome');
        /*----------------------------*/
        wp_register_style(
            'bootstrap_progressbar',
            get_bloginfo('stylesheet_directory') . '/core/css/bootstrap-progressbar-3.3.0.min.css'
        );
        wp_enqueue_style('bootstrap_progressbar');
        /*----------------------------*/
        wp_register_style(
            'flexslider',
            get_bloginfo('stylesheet_directory') . '/core/css/flexslider.css'
        );
        wp_enqueue_style('flexslider');
        /*----------------------------*/
        wp_register_style(
            'slick_css',
            get_bloginfo('stylesheet_directory') . '/core/css/slick.css'
        );
        wp_enqueue_style('slick_css');
        /*----------------------------*/
        wp_register_style(
            'theme_css',
            get_bloginfo('stylesheet_directory') . '/core/css/theme.min.css'
        );
        wp_enqueue_style('theme_css');
        /*----------------------------*/

        /*-------------- Js --------------*/
        //wp_deregister_script('jquery'); // deregister local jquery
        wp_enqueue_script('jquery', get_template_directory_uri() . '/core/js/jquery.min.js', null, null, true);
        wp_enqueue_script('bootstrap', get_template_directory_uri() . '/core/js/bootstrap.min.js', null, null, true);
        wp_enqueue_script('html5shiv', get_template_directory_uri() . '/core/js/html5shiv.js', 'jquery', null, null, true);
        wp_enqueue_script('imagesloaded', get_template_directory_uri() . '/core/js/imagesloaded.pkgd.min.js', null, null, true);
        wp_enqueue_script('jqueryknob', get_template_directory_uri() . '/core/js/jquery.knob.min.js', null, null, true);
        wp_enqueue_script('progressbar', get_template_directory_uri() . '/core/js/bootstrap-progressbar.min.js', null, null, true);
        wp_enqueue_script('numscroller', get_template_directory_uri() . '/core/js/numscroller-1.0.js', null, null, true);
        wp_enqueue_script('isotope', get_template_directory_uri() . '/core/js/isotope.pkgd.min.js', null, null, true);
        wp_enqueue_script('isotope_packery', get_template_directory_uri() . '/core/js/packery-mode.pkgd.min.js', array('isotope'), null, true);      
        wp_enqueue_script('googleapis', 'http://maps.googleapis.com/maps/api/js', null, null, true);
        wp_enqueue_script('gmap', get_template_directory_uri() . '/core/js/jquery.gomap-1.3.3.min.js', null, null, true);
        wp_enqueue_script('flexslider', get_template_directory_uri() . '/core/js/jquery.flexslider-min.js', null, null, true);
        wp_enqueue_script('numscroller', get_template_directory_uri() . '/core/js/numscroller-1.0.js', null, null, true);
        wp_enqueue_script('wow', get_template_directory_uri() . '/core/js/wow.min.js', null, null, true);
        wp_enqueue_script('slick', get_template_directory_uri() . '/core/js/slick.min.js', null, null, true);
        wp_enqueue_script('main', get_template_directory_uri() . '/core/js/main.js', null, null, true);        
    }
}
add_action('init', 'integrita_register_scripts');
/* -----------------------------------------------------
Backend javascripts and css loader
----------------------------------------------------- */