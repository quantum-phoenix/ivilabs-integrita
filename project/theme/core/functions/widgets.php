<?php

/* -----------------------------------------------------
  Widgets
----------------------------------------------------- */
add_action('widgets_init', 'integrita_register_sidebars');

function integrita_register_sidebars() {
    if (function_exists('register_sidebar')) {

        register_sidebar(array(
            'name' => __('Posts Widgets'),
            'id' => 'posts-widgets',
            'description' => __('These are widgets for the sidebar.', 'integrita'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h4>',
            'after_title' => '</h4>'
        ));

        register_sidebar(array(
            'name' => __('Page Widgets'),
            'id' => 'page-widgets',
            'description' => __('These are widgets for the sidebar.', 'integrita'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h4>',
            'after_title' => '</h4>'
        ));

        register_sidebar(array(
            'name' => __('Portfolio Widgets'),
            'id' => 'portfolio-widgets',
            'description' => __('These are widgets for the sidebar.', 'integrita'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h4>',
            'after_title' => '</h4>'
        ));

        register_sidebar(array(
            'name' => __('Footer Widgets'),
            'id' => 'footer-widgets',
            'description' => __('These are widgets for the sidebar.', 'integrita'),
            'before_widget' => '<div id="%1$s" class="widget %2$s col-md-3">',
            'after_widget' => '</div>',
            'before_title' => '<h4>',
            'after_title' => '</h4>'
        ));

        register_sidebar(array(
            'name' => __('Contact Widgets'),
            'id' => 'contact-widgets',
            'description' => __('These are widgets for the sidebar.', 'integrita'),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h4>',
            'after_title' => '</h4>'
        ));
    }
}

?>
