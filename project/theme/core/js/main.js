$j=jQuery.noConflict();
(function($j) {
    $j.fn.hasAttr = function(name) {
        return this.attr(name) !== undefined && this.attr(name) !== false && this.attr(name) != ""; // IE7 Support? http://community.sitepoint.com/t/$j-how-to-identify-missing-vs-empty-attribute-value/3941/3
    }
    $j.fn.hasColor = function(color){
          return (typeof color === "string") && color.length === 6 && ! isNaN( parseInt(color, 16) );
    } 
    $j.fn.validAttrArr = function(arr) {
        if (Array.isArray(arr) && arr.length == 2 && $j.isNumeric(arr[0]) && $j.isNumeric(arr[1])) {
            return true;
        } else {
            return false;
        }
    }
    $j.fn.isInt = function(value) {
      return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value));
    }
    $j.fn.makeGmap = function() {
        var google_map_position_arr;
        var google_map_marker_arr;
        var google_map_title;
        if (this.hasAttr("data-position")) {
            google_map_position_arr = this.attr("data-position").split(",");
        }else{
            google_map_position_arr = false;
        }
        if (this.hasAttr("data-marker")) {
            google_map_marker_arr = this.attr("data-marker").split(",");
        }else{
            google_map_marker_arr = false;
        }
        if (this.hasAttr("data-title")) {
            google_map_title = this.attr("data-title");
        }else{
            google_map_title = "Position";
        }
        /********************************************/
        if (this.validAttrArr(google_map_position_arr) && !this.validAttrArr(google_map_marker_arr)) {
            this.goMap({ 
                latitude: google_map_position_arr[0], 
                longitude: google_map_position_arr[1],
                zoom: 6 
            }); 
        } else if (this.validAttrArr(google_map_marker_arr)) {
            this.goMap({
                latitude: google_map_marker_arr[0],
                longitude: google_map_marker_arr[1],
                zoom: 2,
                markers: [{
                    latitude: google_map_marker_arr[0],
                    longitude: google_map_marker_arr[1],
                    html: { 
                        content: google_map_title, 
                        popup:true 
                    }
                }]
            });
        }
        return this;
    }

}($j));

$j(document).ready(function() {
    "use strict";
    var timerId = 0,
    	i = 0,
        knobDelay = 10;
    var knobSelector = '.dial';
    $j(knobSelector).knob({
        'min': 0,
        'max': 100,
        "readOnly": true,
        "fgColor": "#FF0000",
        "cursor": false,
    });

    if ($j(knobSelector).length) {
	    timerId = setInterval(function() {
	        i++;
	        $j(knobSelector).val(i).trigger('change');
	        if (i == 100) {
	            clearInterval(timerId);
	        }
	    }, knobDelay);
    }

    $j('.progress .progress-bar').progressbar({
        display_text: 'fill'
    });

    var google_map = $j(".gmap");
    if (google_map.length) {

        google_map.each(function() {
           $j(this).makeGmap();
        });

    }
    /*********************************/
    $j(".content-box-footer").each(function() {
        if($j(this).hasAttr("data-background-color")){
            var color = $j(this).attr("data-background-color");
            color = color.substring(1, color.length); // Remove the first # character.
            if ($j(this).hasColor(color)){ $j(this).css("background-color", "#" + color); }                       
        }
    });

    $j('[data-toggle="popover"]').popover({
        trigger : 'hover',
        delay: { "show": 500, "hide": 100 }
    });




    var $jcontainer = $j('.isotope-portfolio');

    $jcontainer.isotope({
        itemSelector: '.portfolio-item',
        layoutMode: 'fitRows'
    });
 
    $j('.isotope-filters button').click(function(){
        $j('.portfolioFilter .is-checked').removeClass('is-checked');
        $j(this).addClass('is-checked');
 
        var selector = $j(this).attr('data-filter');
        $jcontainer.isotope({
            filter: selector,
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
         });
         return false;
    });


    $j('.flexslider-normal').flexslider({
        animation: "slide",
        controlNav: "thumbnails"
    });

    var slick_carousel_column = parseInt($j('.slick-carousel').attr("data-column-count"));
    var slick_carousel_column_width;

    switch (slick_carousel_column) {
        case 1:
            slick_carousel_column_width = 1140;
            break;
        case 2:
            slick_carousel_column_width = 555;
            break;
        case 3:
            slick_carousel_column_width = 350;
            break;
        case 4:
            slick_carousel_column_width = 255;
            break;
        case 6:
            slick_carousel_column_width = 160;
            break;
        default:
            slick_carousel_column_width = 255;
    }
    console.log(slick_carousel_column);
    //if(!$j.isInt(slick_carousel_column)) { slick_carousel_column = 4; }
    $j('.slick-carousel').slick({
        infinite: true,
        slidesToShow: slick_carousel_column,
        slidesToScroll: 1,
        prevArrow: '<div class="slick-prev"><i class="fa fa-angle-left"></i></div>',
        nextArrow: '<div class="slick-next"><i class="fa fa-angle-right"></i></div>'
    });

    $j('.blog-masonry-post-container').isotope({
        layoutMode: 'packery',
        itemSelector: '.blog-masonry-post-item',
        packery: {
          columnWidth: '.blog-masonry-post-item'
        }
    });

    var wow = new WOW({
        animateClass: 'animated',
        offset: 100
    });
    wow.init();
    
});