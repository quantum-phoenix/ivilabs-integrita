<?php get_header(); ?>

<!-- CONTENT-CONTAINER -->
<div class="container">

    <!-- CONTENT -->
    <div class="content">

        <!-- ROW -->
        <div class="row">

            <!-- COLUMN 12 -->
            <div id="breadcrumb" class="breadcrumb col-md-12">
                <?php the_breadcrumb(); ?>
            </div>
            <!-- COLUMN 12 -->

        </div>
        <!-- /ROW -->

        <?php if(get_post_meta(get_the_ID(), 'portfolio_single_layout_meta_box', true) == 'normal_full'): ?>

        <!-- ROW -->
        <div class="row">

            <!-- COLUMN 12 -->
            <div class="col-md-12">

                <!-- START OF LOOP -->
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <!-- PORTFOLIO CONTROLS -->            
                    <div class="portfolio-pagination">               
                        <?php previous_post_link('%link', '<i class="icon-left-open"></i>') ?>             
                        <?php next_post_link('%link' , '<i class="icon-right-open"></i>') ?>
                    </div>
                    <!-- /PORTFOLIO CONTROLS -->

                    <?php $portfolio_multi_image = get_post_meta(get_the_ID(), 'portfolio_multi_image_data', true); ?>

                    <!-- PAGE CONTAINER -->
                    <div class="post-container">

                        <?php if(!empty($portfolio_multi_image)): ?>
                        <div class="flexslider">
                            <ul class="slides">
                            <?php
                            $normal_image_params = array('width' => 1140, 'height' => 504);
                            $thumb_image_params = array('width' => 189, 'height' => 119);
                            foreach ( $portfolio_multi_image as $image_url): 
                                foreach ( $image_url as $img_src):
                                    $thumb_image = bfi_thumb($img_src, $thumb_image_params);
                                    $normal_image = bfi_thumb($img_src, $normal_image_params);
                                    ?>
                                    <li data-thumb="<?php echo $normal_image; ?>">
                                        <img src="<?php echo $normal_image; ?>" alt="<?php echo basename($normal_image); ?>"> 
                                    </li>
                                    <?php                 
                                endforeach;
                            endforeach;
                            ?>
                            </ul>
                        </div>
                        <?php endif; ?>

                        <!-- TITLE-DIV -->
                        <div class="title-div">
                            <h1 class="title">
                                <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                            </h1>
                        </div>
                        <!-- /TITLE-DIV -->

                        <!-- POST-$ID -->
                        <div id="post-<?php the_ID(); ?>" <?php post_class('post-div') ?>>
                            <div class="post-entry">
                                <?php the_content(); ?>
                            </div>
                        </div>
                        <!-- /POST-$ID -->

                    </div>
                    <!-- /PAGE CONTAINER -->

                <?php endwhile; ?>
                <?php endif; ?>
                <!-- /END OF LOOP -->

                <?php comments_template('', true); ?>

            </div>
            <!-- /COLUMN 12 -->

        </div>
        <!-- /ROW -->

        <?php elseif(get_post_meta(get_the_ID(), 'portfolio_single_layout_meta_box', true) == 'normal_left_sidebar'): ?>

        <!-- ROW -->
        <div class="row">

            <!-- COLUMN 4 -->
            <div class="sidebars col-md-4">
                <?php get_sidebar('posts'); ?>
            </div>
            <!-- /COLUMN 4 -->

            <!-- COLUMN 12 -->
            <div class="col-md-8">

                <!-- START OF LOOP -->
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <!-- PORTFOLIO CONTROLS -->            
                    <div class="portfolio-pagination">               
                        <?php previous_post_link('%link', '<i class="icon-left-open"></i>') ?>             
                        <?php next_post_link('%link' , '<i class="icon-right-open"></i>') ?>
                    </div>
                    <!-- /PORTFOLIO CONTROLS -->

                    <?php $portfolio_multi_image = get_post_meta(get_the_ID(), 'portfolio_multi_image_data', true); ?>

                    <!-- PAGE CONTAINER -->
                    <div class="post-container">

                        <?php if(!empty($portfolio_multi_image)): ?>
                        <div class="flexslider">
                            <ul class="slides">
                            <?php
                            $normal_image_params = array('width' => 1140, 'height' => 504);
                            $thumb_image_params = array('width' => 189, 'height' => 119);
                            foreach ( $portfolio_multi_image as $image_url): 
                                foreach ( $image_url as $img_src):
                                    $thumb_image = bfi_thumb($img_src, $thumb_image_params);
                                    $normal_image = bfi_thumb($img_src, $normal_image_params);
                                    ?>
                                    <li data-thumb="<?php echo $normal_image; ?>">
                                        <img src="<?php echo $normal_image; ?>" alt="<?php echo basename($normal_image); ?>"> 
                                    </li>
                                    <?php                 
                                endforeach;
                            endforeach;
                            ?>
                            </ul>
                        </div>
                        <?php endif; ?>

                        <!-- TITLE-DIV -->
                        <div class="title-div">
                            <h1 class="title">
                                <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                            </h1>
                        </div>
                        <!-- /TITLE-DIV -->                        

                        <!-- POST-$ID -->
                        <div id="post-<?php the_ID(); ?>" <?php post_class('post-div') ?>>
                            <div class="post-entry">
                                <?php the_content(); ?>
                            </div>
                        </div>
                        <!-- /POST-$ID -->

                    </div>
                    <!-- /PAGE CONTAINER -->

                <?php endwhile; ?>
                <?php endif; ?>
                <!-- /END OF LOOP -->

                <?php comments_template('', true); ?>

            </div>
            <!-- /COLUMN 12 -->

        </div>
        <!-- /ROW -->

        <?php elseif(get_post_meta(get_the_ID(), 'portfolio_single_layout_meta_box', true) == 'normal_full_paged'): ?>

        <!-- ROW -->
        <div class="row">

            <!-- COLUMN 12 -->
            <div class="col-md-12">

                <!-- START OF LOOP -->
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <!-- PORTFOLIO CONTROLS -->            
                    <div class="portfolio-pagination">               
                        <?php previous_post_link('%link', '<i class="icon-left-open"></i><p>Prev</p>') ?>             
                        <?php next_post_link('%link' , '<i class="icon-right-open"></i><p>Next</p>') ?>
                    </div>
                    <!-- /PORTFOLIO CONTROLS -->

                    <?php $portfolio_multi_image = get_post_meta(get_the_ID(), 'portfolio_multi_image_data', true); ?>
                    <pre>
                        <?php print_r($portfolio_multi_image); ?>
                    </pre>

                    <!-- PAGE CONTAINER -->
                    <div class="post-container">

                        <?php if(!empty($portfolio_multi_image)): ?>
                        <div class="flexslider">
                            <ul class="slides">
                            <?php
                            $normal_image_params = array('width' => 1140, 'height' => 504);
                            $thumb_image_params = array('width' => 189, 'height' => 119);
                            foreach ( $portfolio_multi_image as $image_url): 
                                foreach ( $image_url as $img_src):
                                    $thumb_image = bfi_thumb($img_src, $thumb_image_params);
                                    $normal_image = bfi_thumb($img_src, $normal_image_params);
                                    ?>
                                    <li data-thumb="<?php echo $normal_image; ?>">
                                        <img src="<?php echo $normal_image; ?>" alt="<?php echo basename($normal_image); ?>"> 
                                    </li>
                                    <?php                 
                                endforeach;
                            endforeach;
                            ?>
                            </ul>
                        </div>
                        <?php endif; ?>

                        <!-- TITLE-DIV -->
                        <div class="title-div">
                            <h1 class="title">
                                <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                            </h1>
                        </div>
                        <!-- /TITLE-DIV -->
                        
                        <!-- POST-$ID -->
                        <div id="post-<?php the_ID(); ?>" <?php post_class('post-div') ?>>
                            <div class="post-entry">
                                <?php the_content(); ?>
                            </div>
                        </div>
                        <!-- /POST-$ID -->

                    </div>
                    <!-- /PAGE CONTAINER -->

                <?php endwhile; ?>
                <?php endif; ?>
                <!-- /END OF LOOP -->

                <?php comments_template('', true); ?>

            </div>
            <!-- /COLUMN 12 -->

        </div>
        <!-- /ROW -->

        <?php endif; ?>
    </div>
    <!-- /CONTENT -->

</div>
<!-- /CONTENT-CONTAINER -->

<?php get_footer(); ?>