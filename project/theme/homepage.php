<?php
/**
 * Template Name: Homepage Template
 */
?>

<?php get_header(); ?>

<!-- ROW -->
<div class="row head-content">

    <!-- CONTENT-CONTAINER -->
    <div class="container">

        <!-- COLUMN 6 -->
        <div class="col-md-6">
            <div class="title-div">                                
                <h3 class="no-margin"><?php the_title(); ?></h3>                                
            </div>
        </div>
        <!-- /COLUMN 6 -->

        <!-- COLUMN 6 -->
        <div class="int-breadcrumb col-md-6">
            <?php the_breadcrumb(); ?>
        </div>
        <!-- /COLUMN 6 -->
        
    </div>
    <!-- /CONTENT-CONTAINER -->

</div>
<!-- /ROW -->

<!-- CONTENT-CONTAINER -->
<div class="container">

    <!-- CONTENT -->
    <div class="content">

        <!-- COLUMN 12 -->
        <div class="col-md-12">

            <!-- START OF LOOP -->
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <!-- POST CONTAINER -->
                <div class="page-container">

                    <!-- POST-$ID -->
                    <div id="post-<?php the_ID(); ?>" <?php post_class('page-div') ?>>
                        <div class="page-entry">
                            <?php the_content(); ?>
                        </div>
                    </div>
                    <!-- /POST-$ID -->

                </div>
                <!-- /POST CONTAINER -->

            <?php endwhile; ?>
            <?php endif; ?>
            <!-- /END OF LOOP -->

        </div>
        <!-- COLUMN 12 -->

    </div>
    <!-- /CONTENT -->

</div>
<!-- /CONTENT-CONTAINER -->

<?php get_footer(); ?>