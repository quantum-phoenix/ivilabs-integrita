<?php
/**
 * Template Name: Portfolio List Without Sidebar Layout
 */
?>

<?php get_header(); ?>

<!-- CONTENT-CONTAINER -->
<div class="container">

    <!-- CONTENT -->
    <div class="content">
          
            <!-- ROW -->
            <div class="row">

                <!-- COLUMN 12 -->
                <div class="col-md-12">

                    <!-- START OF LOOP -->
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>                        

                        <!-- PAGE-CONTAINER -->
                        <div class="page-container">

                            <!-- TITLE-DIV -->
                            <div class="title-div">                                
                                <h3><?php the_title(); ?></h3>                           
                            </div>
                            <!-- /TITLE-DIV -->

                        </div>
                        <!-- /PAGE-CONTAINER -->

                        <!-- PAGE-CONTAINER -->
                        <div class="page-container">
                            <?php get_template_part( 'core/templates/portfolio-list', 'content' ); ?>
                        </div>
                        <!-- /PAGE-CONTAINER -->

                    <?php endwhile; ?>
                    <?php endif; ?>
                    <!-- /END OF LOOP -->

                </div>
                <!-- /COLUMN 12 -->

            </div>
            <!-- /ROW -->

    </div>
    <!-- /CONTENT -->

</div>
<!-- /CONTENT-CONTAINER -->

<?php get_footer(); ?>