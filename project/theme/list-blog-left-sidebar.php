<?php
/**
 * Template Name: List Blog Left Sidebar
 */
?>

<?php get_header(); ?>

<!-- ROW -->
<div class="row head-content">

    <!-- CONTENT-CONTAINER -->
    <div class="container">

        <!-- COLUMN 6 -->
        <div class="col-md-6">
            <div class="title-div">                                
                <h3 class="no-margin"><?php the_title(); ?></h3>                                
            </div>
        </div>
        <!-- /COLUMN 6 -->

        <!-- COLUMN 6 -->
        <div class="int-breadcrumb col-md-6">
            <?php the_breadcrumb(); ?>
        </div>
        <!-- /COLUMN 6 -->
        
    </div>
    <!-- /CONTENT-CONTAINER -->

</div>
<!-- /ROW -->

<!-- CONTENT-CONTAINER -->
<div class="container">

    <!-- CONTENT -->
    <div class="content">
          
            <!-- ROW -->
            <div class="row">

                <!-- COLUMN 2 -->
                <div class="col-md-3">
                    <div class="sidebars">
                        <?php get_sidebar('posts'); ?>
                    </div>
                </div>
                <!-- /COLUMN 2 -->  
                
                <!-- COLUMN 10 -->
                <div class="col-md-9">

                    <!-- START OF LOOP -->
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        
                        <!-- PAGE-CONTAINER -->
                        <div class="page-container">
                            <?php get_template_part( 'core/templates/blog-list', 'content' ); ?>
                        </div>
                        <!-- /PAGE-CONTAINER -->

                    <?php endwhile; ?>

                        <!-- NAV -->
                        <div id="nav-above" class="navigation blog-navigation">
                            <div class="nav-previous">
                                <?php next_posts_link(__('<p>Older posts</p><span class="meta-nav-new"></span>')); ?>
                            </div>

                            <div class="nav-next">
                                <?php previous_posts_link(__('<p>Newer posts</p><span class="meta-nav-old"></span>')); ?>
                            </div>
                        </div>
                        <!-- /NAV -->

                    <?php endif; ?>
                    <!-- /END OF LOOP -->

                </div>
                <!-- /COLUMN 10 -->

            </div>
            <!-- /ROW -->

    </div>
    <!-- /CONTENT -->

</div>
<!-- /CONTENT-CONTAINER -->

<?php get_footer(); ?>