<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 */

function optionsframework_option_name() {
	// Change this to use your theme slug
	return 'integrita';
}	


/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'theme-textdomain'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	$sidebars_array = array(
		'left_sidebar' => __( 'Left sidebar', 'integrita' ),
		'right_sidebar' => __( 'Right sidebar', 'integrita' ),
		'without_sidebar' => __( 'Without sidebar', 'integrita' )
	);

	$column_count_array = array(
		'two_column' => __( 'Two column', 'integrita' ),
		'three_column' => __( 'Three column', 'integrita' ),
		'four_column' => __( 'Four column', 'integrita' )
	);

	$options = array();

	$options[] = array(
		'name' => __( 'General Options', 'integrita' ),
		'type' => 'heading'
	);

	$options[] = array(
		'name' => __( 'Logo upload', 'integrita' ),
		'desc' => __( 'Itt tudod feltölteni az oldal logoját..', 'theme-textdomain' ),
		'id' => 'logo_upload',
		'type' => 'upload'
	);

	$options[] = array(
		'name' => __( 'Templates Options', 'integrita' ),
		'type' => 'heading'
	);

	$options[] = array(
		'name' => __( 'Sidebar at Single', 'integrita' ),
		'desc' => __( 'Sidebar at Single', 'integrita' ),
		'id' => 'single_sidebar_select',
		'std' => 'three',
		'type' => 'select',
		'class' => 'mini', //mini, tiny, small
		'options' => $sidebars_array
	);

	$options[] = array(
		'name' => __( 'Column count in grid portfolio', 'integrita' ),
		'desc' => __( 'Column count in grid portfolio', 'integrita' ),
		'id' => 'portfolio_column_count_select',
		'std' => 'three',
		'type' => 'select',
		'class' => 'mini', //mini, tiny, small
		'options' => $column_count_array
	);

	$options[] = array(
		'name' => __( 'Column count in masonry blog', 'integrita' ),
		'desc' => __( 'Column count in masonry blog', 'integrita' ),
		'id' => 'masonry_blog_column_count_select',
		'std' => 'three',
		'type' => 'select',
		'class' => 'mini', //mini, tiny, small
		'options' => $column_count_array
	);

	return $options;
}
