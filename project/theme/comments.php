<?php if (post_password_required()) : ?>
    <div id="need-password-div">
        <p class="need-password"><?php _e('This post is password protected. Enter the password to view any comments.', 'integrita'); ?></p>
    </div>
<?php endif; ?>

<?php if (have_comments()) : ?>

<div class="subtitle-comments">
    <h2><?php _e('Comments', 'integrita'); ?> (<?php comments_number('0', '1', '%'); ?>)</h2>
</div>

<?php $wp_list_comments_args = array(
        'walker' => null,
        'max_depth' => '',
        'style' => 'ul',
        'callback' => 'integrita_comment',
        'end-callback' => null,
        'type' => 'all',
        'page' => '',
        'per_page' => '',
        'avatar_size' => 45,
        'reverse_top_level' => null,
        'reverse_children' => ''); ?>

<ol class="commentlist">
    <?php wp_list_comments($wp_list_comments_args); ?>
</ol>

<?php if (get_comment_pages_count() > 1) : ?>
    <nav id="comment-nav">
        <div class="nav-previous"><?php previous_comments_link(__('Older Comments', 'integrita')); ?></div>
        <div class="nav-next"><?php next_comments_link(__('Newer Comments', 'integrita')); ?></div>
    </nav>
<?php endif; ?>

<?php elseif (!comments_open() && !is_page() && post_type_supports(get_post_type(), 'comments')) : ?>
    <p class="no_comments"><?php _e('Comments are closed.', 'integrita'); ?></p>
<?php endif; ?>


<?php
$commenter = wp_get_current_commenter();
$req = get_option('require-name-email');
$aria_req = ($req ? " aria-required='true'" : '');
$fields = array(
    'author' => '<div class="row clearfix"><div class="four_box"><p class="comment-form-authorx">' .
        '<label for="author">' . __('Name') . '</label> ' . ($req ? '<span class="required">*</span>' : '') .
        '<input id="author" name="author" type="text" value="' . esc_attr($commenter['comment_author']) . '" size="30"' . $aria_req . ' />
                </p></div>',
    'email' => '<div class="four_box"><p class="comment-form-email"><label for="email">' . __('Email') . '</label> ' . ($req ? '<span class="required">*</span>' : '') .
        '<input id="email" name="email" type="text" value="' . esc_attr($commenter['comment_author_email']) . '" size="30"' . $aria_req . ' />
                </p></div></div>',
);

$comments_args = array(
    'fields' => $fields,
    'title_reply' => '<h2>' . __('Leave a reply', 'integrita') . '</h2>',
    'label_submit' => 'Submit',
    'comment_notes_after' => ''
);

comment_form($comments_args);

?>

