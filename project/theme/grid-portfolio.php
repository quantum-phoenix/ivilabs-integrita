<?php
/**
 * Template Name: Grid Portfolio Layout
 */
?>

<?php get_header(); ?>

<!-- CONTENT-CONTAINER -->
<div class="container">

    <!-- CONTENT -->
    <div class="content">
            <?php
            $terms = get_terms("portfolio_taxonomy");
            $count = count($terms);  
            $html_string = '';                       
            ?>
            <?php if ( $count > 0 ): ?>
            <!-- ROW -->
            <div class="row">
                <div class="col-lg-12">

                    <div class="isotope-filters clearfix">
                        <?php                        
                        $allString = __("All" , "sentient");
                        $html_string .= '<button class="button" data-filter="*">' . $allString . '</button>';
                        foreach ( $terms as $term ):  
                            if($term->name != 'Uncategorized' && $term->name != 'uncategorized'):
                                $termname = strtolower($term->name);  
                                $termname = str_replace(' ', '-', $termname);  
                                $html_string .= '<button class="button" data-filter=".'.$termname.'">'.$term->name.'</button>';  
                            endif;
                        endforeach;                
                        echo $html_string;        
                        ?> 
                    </div>

                </div>
            </div> 
            <!-- /ROW -->
            <?php endif; ?>

            <!-- ROW -->
            <div class="row">

                <?php $column_count_value = of_get_option( 'portfolio_column_count_select', 'two_column' ); ?>
                            
                <!-- COLUMN 12 -->
                <div class="col-md-12">

                    <!-- ISOTOPE PORTFOLIO -->
                    <div class="isotope-portfolio">

                        <?php $args = array( 'post_type' => 'portfolio', 'posts_per_page' => 10, 'post_status' => 'publish', 'orderby' => 'post_date', 'order' => 'DESC'); ?>
                        <?php $loop = new WP_Query( $args ); ?>

                        <!-- START OF LOOP -->
                        <?php 
                        if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();

                            $terms = get_the_terms( get_the_ID() , 'portfolio_taxonomy' );                                                               
                            if ( $terms && ! is_wp_error( $terms ) ) :   
                                $links = array();
                                foreach ( $terms as $term ):  
                                    $links[] = $term->name;  
                                endforeach;  
                                $links = str_replace(' ', '-', $links);   
                                $tax = join( " ", $links );       
                            else :    
                                $tax = '';    
                            endif;

                            $tax = strtolower($tax);
                            ?>

                            <!-- PORTFOLIO-ITEM -->
                            <div class="portfolio-item <?php echo $column_count_value; ?>-grid-portfolio-item <?php echo esc_attr($tax); ?>">

                                <!-- TITLE-DIV -->
                                <div class="title-div">
                                    <h1 class="title">
                                        <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                                    </h1>
                                </div>
                                <!-- /TITLE-DIV -->

                                <?php $portfolio_multi_image = get_post_meta(get_the_ID(), 'portfolio_multi_image_data', true); ?>
                                <?php if(!empty($portfolio_multi_image)): ?>
                                <div>  
                                    <?php
                                    $thumb_image_params = array('width' => 380, 'height' => 380);
                                    $img_src = $portfolio_multi_image['image_url'][0];
                                    $thumb_image = bfi_thumb($img_src, $thumb_image_params);
                                    ?>                                      
                                    <img src="<?php echo $thumb_image; ?>" alt="<?php echo basename($thumb_image); ?>">                                         
                                </div>
                                <?php endif; ?>

                                <!-- POST-$ID -->
                                <div id="post-<?php the_ID(); ?>" <?php post_class('post-div') ?>>
                                    <div class="excerpt">
                                        <?php the_excerpt(); ?>
                                    </div>
                                </div>
                                <!-- /POST-$ID -->

                            </div>
                            <!-- /PORTFOLIO-ITEM -->

                        <?php endwhile; ?>

                            <!-- NAV -->
                            <div id="nav-above" class="navigation blog-navigation">
                                <div class="nav-previous">
                                    <?php next_posts_link(__('<p>Older posts</p><span class="meta-nav-new"></span>')); ?>
                                </div>

                                <div class="nav-next">
                                    <?php previous_posts_link(__('<p>Newer posts</p><span class="meta-nav-old"></span>')); ?>
                                </div>
                            </div>
                            <!-- /NAV -->

                        <?php endif; ?>
                        <!-- /END OF LOOP -->

                    </div>
                    <!-- /ISOTOPE PORTFOLIO -->

                </div>
                <!-- /COLUMN 12 -->

            </div>
            <!-- /ROW -->

    </div>
    <!-- /CONTENT -->

</div>
<!-- /CONTENT-CONTAINER -->

<?php get_footer(); ?>
