<?php
/**
 * Template Name: Page Right Sidebar Template
 */
?>

<?php get_header(); ?>

<!-- CONTENT-CONTAINER -->
<div class="container">

    <!-- CONTENT -->
    <div class="content">
    
        <!-- ROW -->
        <div class="row">

            <!-- COLUMN 12 -->
            <div id="breadcrumb" class="breadcrumb col-md-12">
                <?php the_breadcrumb(); ?>
            </div>
            <!-- COLUMN 12 -->

        </div>
        <!-- /ROW -->

        <!-- ROW -->
        <div class="row">           

            <!-- COLUMN 9 -->
            <div class="col-md-9">

                <!-- START OF LOOP -->
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <!-- PAGE-CONTAINER -->
                    <div class="page-container">

                        <!-- TITLE-DIV -->
                        <div class="title-div">
                            <h1 class="title">
                                <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                            </h1>
                        </div>
                        <!-- /TITLE-DIV -->

                        <!-- START OF THUMBNAIL LOOP -->
                        <?php if (has_post_thumbnail()) : ?>
                            <div class="blog-thumb">
                                <a href="<?php the_permalink() ?>"><?php the_post_thumbnail('blog-thumb'); ?></a>
                            </div>
                        <?php endif; ?>
                        <!-- /END OF THUMBNAIL LOOP -->

                        <!-- POST-$ID -->
                        <div id="post-<?php the_ID(); ?>" <?php post_class('page-div') ?>>
                            <div class="page-entry">
                                <?php the_content(); ?>
                            </div>
                        </div>
                        <!-- /POST-$ID -->

                    </div>
                    <!-- /PAGE-CONTAINER -->

                <?php endwhile; ?>
                <?php endif; ?>
                <!-- /END OF LOOP -->

            </div>
            <!-- /COLUMN 9 -->

            <!-- COLUMN 3 -->
            <div class="col-md-3">
                <div class="sidebars">
                    <?php get_sidebar('page'); ?>
                </div>
            </div>
            <!-- /COLUMN 3 -->
            
        </div>
        <!-- /ROW -->

    </div>
    <!-- /CONTENT -->

</div>
<!-- /CONTENT-CONTAINER -->

<?php get_footer(); ?>