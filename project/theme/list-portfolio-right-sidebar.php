<?php
/**
 * Template Name: Portfolio List Right Sidebar Layout
 */
?>

<?php get_header(); ?>

<!-- CONTENT-CONTAINER -->
<div class="container">

    <!-- CONTENT -->
    <div class="content">
          
            <!-- ROW -->
            <div class="row">

                <!-- COLUMN 10 -->
                <div class="col-md-10">

                    <!-- START OF LOOP -->
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>                        

                        <!-- PAGE-CONTAINER -->
                        <div class="page-container">

                            <!-- TITLE-DIV -->
                            <div class="title-div">                                
                                <h3><?php the_title(); ?></h3>                           
                            </div>
                            <!-- /TITLE-DIV -->

                        </div>
                        <!-- /PAGE-CONTAINER -->

                        <!-- PAGE-CONTAINER -->
                        <div class="page-container blog-masonry-post-container clearfix">
                            <?php get_template_part( 'core/templates/portfolio-list', 'content' ); ?>
                        </div>
                        <!-- /PAGE-CONTAINER -->

                    <?php endwhile; ?>
                    <?php endif; ?>
                    <!-- /END OF LOOP -->

                </div>
                <!-- /COLUMN 10 -->

                <!-- COLUMN 2 -->
                <div class="col-md-2">
                    <div class="sidebars">
                        <?php get_sidebar('portfolio'); ?>
                    </div>
                </div>
                <!-- /COLUMN 2 -->

            </div>
            <!-- /ROW -->

    </div>
    <!-- /CONTENT -->

</div>
<!-- /CONTENT-CONTAINER -->

<?php get_footer(); ?>