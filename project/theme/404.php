<?php get_header(); ?>

<!-- CONTENT-CONTAINER -->
<div class="container">

    <!-- CONTENT -->
    <div class="content">
        
        <!-- ROW -->
        <div class="row">

            <!-- COLUMN 12 -->
            <div class="col-md-12">

                <!-- PAGE-CONTAINER -->
                <div class="page-container">

                    <p><?php _e("Error 404", "integrita"); ?></p>
                    <?php get_search_form(); ?>

                </div>
                <!-- /PAGE-CONTAINER -->

            </div>
            <!-- /COLUMN 12 -->

        </div>
        <!-- /ROW -->

    </div>
    <!-- /CONTENT -->

</div>
<!-- /CONTENT-CONTAINER -->

<?php get_footer(); ?>