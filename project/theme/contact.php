<?php
/**
 * Template Name: Contact Template
 * Description: The Contact Template
 *
 * @package Integrita default theme
 * @author Rubidium Style Team
 * @link http://rubidium-style.com/
 */
?>

<?php get_header(); ?>

<!-- CONTENT-CONTAINER -->
<div class="container">

    <!-- CONTENT -->
    <div class="content">
        
        <!-- ROW -->
        <div class="row">

            <!-- COLUMN 12 -->
            <div id="breadcrumb" class="breadcrumb col-md-12">
                <?php the_breadcrumb(); ?>
            </div>
            <!-- COLUMN 12 -->
        
        </div>
        <!-- /ROW -->

        <!-- ROW -->
        <div class="row">

            <!-- COLUMN 12 -->
            <div class="col-md-12">

                <!-- START OF LOOP -->
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <!-- POST CONTAINER -->
                    <div class="page-container">

                        <!-- TITLE -->
                        <div class="title-div">
                            <h1 class="title">
                                <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                            </h1>
                        </div>
                        <!-- /TITLE -->

                        <!-- POST-$ID -->
                        <div id="post-<?php the_ID(); ?>" <?php post_class('page-div') ?>>
                            <div class="page-entry">
                                <?php the_content(); ?>
                            </div>
                        </div>
                        <!-- /POST-$ID -->

                    </div>
                    <!-- /POST CONTAINER -->

                <?php endwhile; ?>
                <?php endif; ?>
                <!-- /END OF LOOP -->

            </div>
            <!-- COLUMN 12 -->

        </div>
        <!-- /ROW -->

    </div>
    <!-- /CONTENT -->

</div>
<!-- /CONTENT-CONTAINER -->

<?php get_footer(); ?>