'use strict';
module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-commands');

    grunt.initConfig({
        uglify: {
            min: {
                files: [{
                    src: ["project/less/theme/int_theme.css"],
                    dest: "project/theme/core/css/theme.min.css"
                }]
            }
        },
        copy: {
            my_bootstrap_for_original_bootstrap: {
                files: [{
                    cwd: 'project/less/bootstrap_theme_override/',
                    src: '*',
                    dest: 'bower_components/bootstrap/less/',
                    expand: true
                }, {
                    cwd: 'project/less/theme/',
                    src: 'int_variables.less',
                    dest: 'bower_components/bootstrap/less/',
                    expand: true
                }]
            },
            bower_for_assets: {
                files: [                
                    /*++++++ Jquery ++++++*/
                    {
                        cwd: 'bower_components/jquery/dist/',
                        src: 'jquery.min.js',
                        dest: 'project/theme/core/js/',
                        expand: true
                    }, {
                        cwd: 'bower_components/jquery/dist/',
                        src: 'jquery.min.map',
                        dest: 'project/theme/core/js/',
                        expand: true
                    },
                    /*++++++ Bootstrap ++++++*/
                    {
                        cwd: 'bower_components/bootstrap/dist/js/',
                        src: 'bootstrap.min.js',
                        dest: 'project/theme/core/js/',
                        expand: true
                    }, {
                        cwd: 'bower_components/bootstrap/dist/css/',
                        src: 'bootstrap.min.css',
                        dest: 'project/theme/core/css/',
                        expand: true
                    }, {
                        cwd: 'bower_components/bootstrap/dist/css/',
                        src: 'bootstrap-theme.min.css',
                        dest: 'project/theme/core/css/',
                        expand: true
                    }, {
                        cwd: 'bower_components/bootstrap/dist/css/',
                        src: '*.map',
                        dest: 'project/theme/core/css/',
                        expand: true
                    }, {
                        cwd: 'bower_components/bootstrap/dist/fonts/',
                        src: '*',
                        dest: 'project/theme/core/fonts/',
                        expand: true
                    },
                    /*++++++ Bootstrap social ++++++*/
                    {
                        cwd: 'bower_components/bootstrap-social/',
                        src: 'bootstrap-social.css',
                        dest: 'project/theme/core/css/',
                        expand: true
                    },
                    /*++++++ Bootstrap progress bar ++++++*/
                    {
                        cwd: 'bower_components/bootstrap-progressbar/css/',
                        src: 'bootstrap-progressbar-3.3.0.min.css',
                        dest: 'project/theme/core/css/',
                        expand: true
                    },
                    /*++++++ Bootstrap progress bar ++++++*/
                    {
                        cwd: 'bower_components/bootstrap-progressbar/',
                        src: 'bootstrap-progressbar.min.js',
                        dest: 'project/theme/core/js/',
                        expand: true
                    },
                    /*++++++ Font awesome ++++++*/
                    {
                        cwd: 'bower_components/font-awesome/css/',
                        src: 'font-awesome.min.css',
                        dest: 'project/theme/core/css/',
                        expand: true
                    }, {
                        cwd: 'bower_components/font-awesome/fonts/',
                        src: '*',
                        dest: 'project/theme/core/fonts/',
                        expand: true
                    },
                    /*++++++ jQuery Knob ++++++*/
                    {
                        cwd: 'bower_components/jquery-knob/dist/',
                        src: 'jquery.knob.min.js',
                        dest: 'project/theme/core/js/',
                        expand: true
                    },
                    /*++++++ countUp.js ++++++*/
                    {
                        cwd: 'bower_components/countUp.js/',
                        src: 'countUp.js',
                        dest: 'project/theme/core/js/',
                        expand: true
                    },
                    /*++++++ Isotope ++++++*/
                    {
                        cwd: 'bower_components/isotope/dist/',
                        src: 'isotope.pkgd.min.js',
                        dest: 'project/theme/core/js/',
                        expand: true
                    },
                    /*++++++ Isotope Packery ++++++*/
                    {
                        cwd: 'bower_components/isotope-packery/',
                        src: 'packery-mode.pkgd.min.js',
                        dest: 'project/theme/core/js/',
                        expand: true
                    },
                    /*++++++ NumScroller ++++++*/
                    {
                        cwd: 'offline_components/numscroller/',
                        src: 'numscroller-1.0.js',
                        dest: 'project/theme/core/js/',
                        expand: true
                    },
                    /*++++++ FlexSlider ++++++*/
                    {
                        cwd: 'bower_components/FlexSlider/',
                        src: 'jquery.flexslider-min.js',
                        dest: 'project/theme/core/js/',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/FlexSlider/',
                        src: 'flexslider.css',
                        dest: 'project/theme/core/css/',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/FlexSlider/images/',
                        src: 'bg_play_pause.png',
                        dest: 'project/theme/core/css/images/',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/FlexSlider/fonts/',
                        src: '*',
                        dest: 'project/theme/core/fonts/',
                        expand: true
                    },
                    /*++++++ Numscroller ++++++*/
                    {
                        cwd: 'bower_components/numscroller/',
                        src: 'numscroller-1.0.js',
                        dest: 'project/theme/core/js/',
                        expand: true
                    },
                    /*++++++ Wowjs ++++++*/
                    {
                        cwd: 'bower_components/wowjs/dist/',
                        src: 'wow.min.js',
                        dest: 'project/theme/core/js/',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/wowjs/css/libs/',
                        src: 'animate.css',
                        dest: 'project/theme/core/css/',
                        expand: true
                    },
                    /*++++++ Slick.js ++++++*/
                    {
                        cwd: 'bower_components/slick.js/slick/',
                        src: 'slick.min.js',
                        dest: 'project/theme/core/js/',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/slick.js/slick/',
                        src: 'slick.css',
                        dest: 'project/theme/core/css/',
                        expand: true
                    },
                    {
                        cwd: 'bower_components/slick.js/slick/fonts/',
                        src: '*',
                        dest: 'project/theme/core/css/fonts/',
                        expand: true
                    }
                ]
            }
        },
        cssmin: {
            my_target: {
                files: [{
                    expand: true,
                    cwd: 'project/less/theme',
                    src: 'theme.css',
                    dest: 'project/theme/core/css',
                    ext: '.min.css'
                }]
            }
        },
        watch: {
            files: ['project/less/theme/theme.css'],
            tasks: ['cssmin']
        },
        commands: {
            run_bat: {
                type : 'bat',
                cmd  : 'D:/IviLabs/copy_integrita_into_integrita_wp.bat'
            }
        }
    });

    grunt.registerTask('run-bootstrap-grunt', function() {
        var done = this.async();
        grunt.util.spawn({
            grunt: true,
            args: ['dist'],
            opts: {
                cwd: 'bower_components/bootstrap'
            }
        }, function(err, result, code) {
            if (code === 1) {
                grunt.fail.fatal('Grunt error.\n');
                grunt.log.error(err);
            }
            if (code <= 1) {
                err = null;
            } //codes 0 & 1 are expected, not errors
            done(!err);
        });
    });

    grunt.registerTask('build', ['copy:my_bootstrap_for_original_bootstrap', 'run-bootstrap-grunt', 'copy:bower_for_assets']);
    grunt.registerTask('move', ['copy:bower_for_assets']);

};
